package algorithmicsapplication.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;


public class MainMenu extends ActionBarActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        Button firstTasksPartButton = new Button(this);
        firstTasksPartButton.setText("Задачи первого семестра");
        firstTasksPartButton.setId(525);
        firstTasksPartButton.setOnClickListener(this);

        Button secondTasksPartButton = new Button(this);
        secondTasksPartButton.setText("Задачи второго семестра");
        secondTasksPartButton.setId(3442);
        secondTasksPartButton.setOnClickListener(this);
        LinearLayout layout = (LinearLayout) findViewById(R.id.activity_main_menu);
        layout.addView(firstTasksPartButton);
        layout.addView(secondTasksPartButton);
    }

    public void onClick(View view)
    {
        switch (view.getId()) {

            case 525:
                startActivity(new Intent(MainMenu.this, FirstPartTasksMenu.class));
                break;
            case 3442:
                startActivity(new Intent(MainMenu.this, SecondPartTasksMenu.class));
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
