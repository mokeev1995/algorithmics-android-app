package algorithmicsapplication.app.FirstTasks;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 11/01/2015.
 */
public class JavaScriptMathRender extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.javascript_math_render);

        LinearLayout layout = (LinearLayout) findViewById(R.id.java_script_math_render_layout);
        WebView webView = new WebView(this);
        WebSettings settings =  webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_asset/test.html");
        layout.addView(webView);

    }
}
