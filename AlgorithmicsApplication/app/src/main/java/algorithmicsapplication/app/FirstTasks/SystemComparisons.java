package algorithmicsapplication.app.FirstTasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by Nick on 06/01/2015.
 */
public class SystemComparisons {
    public List<ComparisonByMod> _Comparisons = new ArrayList<ComparisonByMod>();
    public ComparisonByMod ResultingComparison;
    public String[] letters = {"q", "t", "s", "p", "k"};

    public String FindAnswer() {
        String solution = forAllComparisonsreduceToOneCoefficientA();


        Stack<ComparisonByMod> stack = new Stack<ComparisonByMod>();
        solution += "После приведения коэффициентов при 'x' имеем следующую систему сравнений:\n";
        for (int i =  0; i < _Comparisons.size(); i++) {
            solution += _Comparisons.get(i).ToString() + "\n";
        }

        for (int i = _Comparisons.size() - 1; i >= 0; i--) {
            stack.push(_Comparisons.get(i));
        }

        ComparisonByMod insertableComparison = stack.pop();
        ComparisonByMod comparisonForInserting = stack.pop();
        solution += insertableComparison.toDivideCoefficient();
        solution += comparisonForInserting.toDivideCoefficient();
        solution += "-------\nРассматриваются два первых сравнения:\n";
        solution += "x = (" + insertableComparison.bCoefficient + ")(mod " +
                insertableComparison.modValue + ");\n";
        solution += "x = (" + comparisonForInserting.bCoefficient + ")(mod " +
                comparisonForInserting.modValue + ");\n";
        solution += "-------\n";
        int currentIndexLetter = 0;

        while (true) {
            solution += "Подставляем первое из них во второе: \n";
            solution += "(" + insertableComparison.bCoefficient + ") + (" + insertableComparison.modValue +
                    ")  * " + letters[currentIndexLetter] + " = (" + comparisonForInserting.bCoefficient + ") (mod " + comparisonForInserting.modValue + ");\n";

            solution += "(" + insertableComparison.modValue + ") * " + letters[currentIndexLetter] +
                    " = ( (" + comparisonForInserting.bCoefficient + ") - (" +

                    insertableComparison.bCoefficient
                    + ") )(mod " + comparisonForInserting.modValue + ");\n";

            solution += "(" + insertableComparison.modValue + ") * " + letters[currentIndexLetter] +
                    " = ( (" + (comparisonForInserting.bCoefficient - insertableComparison.bCoefficient) + ") )(mod " + comparisonForInserting.modValue + ");\n";

            int addendBcoefficient = PrimeFactorization.getPositiveNumberByMod(
                    comparisonForInserting.bCoefficient - insertableComparison.bCoefficient,
                    comparisonForInserting.modValue);

            solution += "(" + insertableComparison.modValue + ") * " + letters[currentIndexLetter] +
                    " = ( (" + addendBcoefficient + ") )(mod " + comparisonForInserting.modValue + ");\n";

            //ВСТАВИТЬ:
            int middleComparison_A = insertableComparison.modValue;
            int middleComparison_B = PrimeFactorization.getPositiveNumberByMod(comparisonForInserting.bCoefficient -
            insertableComparison.bCoefficient, comparisonForInserting.modValue);
            int middleComparison_Mod = comparisonForInserting.modValue;
            int reversedForA_MiddleComparison = -555555;

            // РАЗДЕЛИТЬ и Найти обратный:

            if(PrimeFactorization.isHaveReversedToNumberByMod(insertableComparison.modValue,
                    comparisonForInserting.modValue))
            {
                reversedForA_MiddleComparison = PrimeFactorization.getReversedToNumberByMod(middleComparison_A, middleComparison_Mod);
            }
            else
            {
                if(PrimeFactorization.egcd(middleComparison_A, middleComparison_B) != 1)
                {
                    solution += "На данном шаге мы имеем: (" + middleComparison_A + ") * " + letters[currentIndexLetter]
                            + " = (" + middleComparison_B + ") (mod " +  middleComparison_Mod + ") и его следует сократить, т.к."
                            + " иначе нельзя найти обратный элемент:\n";
                    int commonDividerAandB = PrimeFactorization.egcd(middleComparison_A, middleComparison_B);
                    int commodDivierModDividerAandB =  PrimeFactorization.egcd(commonDividerAandB, middleComparison_Mod);
                    solution += "Коэффициенты a и b -  можно сократить на '" + commonDividerAandB
                            + "', при этом сократив модуль на '" + commodDivierModDividerAandB + "';\n";
                    middleComparison_A = middleComparison_A/commonDividerAandB;
                    middleComparison_B = middleComparison_B / commonDividerAandB;
                    middleComparison_Mod  = middleComparison_Mod  / commodDivierModDividerAandB;
                    reversedForA_MiddleComparison = PrimeFactorization.getReversedToNumberByMod(middleComparison_A,
                            middleComparison_Mod);
                }
                else
                {
                    solution += "Нельзя сократить коэффициенты, поделив их. ФАТАЛЬНАЯ ОШИБКА!!!\n";
                    return solution;
                }
            }

            //ДОМНОЖИТЬ НА ОБРАТНЫЙ:
//

            int middleComparison_B_AfterMultiplyingOnReversedForA = reversedForA_MiddleComparison * middleComparison_B;

            ComparisonByMod middleComparison =  new ComparisonByMod(1,  middleComparison_B_AfterMultiplyingOnReversedForA, middleComparison_Mod);
            solution += "После умножение на обратный к '" + middleComparison_A + "' - получаем:\n"
                    + letters[currentIndexLetter] + " = (" + middleComparison_B_AfterMultiplyingOnReversedForA
                    + ")(mod " + middleComparison_Mod + ").\n";

            //ComparisonByMod newComparison = insertableComparison.enterToAnotherCopmarison(comparisonForInserting);
            ComparisonByMod newComparison = insertableComparison.enterToAnotherCopmarison( middleComparison);
            solution += "Подставив это сравнение в первое сравнение, получим:\n" +
                    " x = (" + newComparison.bCoefficient + ") (mod " + newComparison.modValue + ").\n";

            stack.push(newComparison);
            if (stack.size() >= 2) {

                insertableComparison = stack.pop();
                comparisonForInserting = stack.pop();
                solution += "-------\nРассматриваются следующие два сравнения:\n";

                solution += "x = (" + insertableComparison.bCoefficient + ")(mod " +
                        insertableComparison.modValue + ");\n";
                solution += "x = (" + comparisonForInserting.bCoefficient + ")(mod " +
                        comparisonForInserting.modValue + ");\n";
                solution += "-------\n";
            }
            if (stack.size() == 1) {
                solution += "x = (" + insertableComparison.bCoefficient + ") + (" + insertableComparison.modValue
                        + ") * ( (" + middleComparison_B + ") + (" + middleComparison_Mod +
                        ") * " + letters[currentIndexLetter + 1] + ") = ";
                solution += "(" +
                        (insertableComparison.bCoefficient +
                                insertableComparison.modValue * middleComparison_B)
                        + ") + " + (insertableComparison.modValue * middleComparison_Mod) + " * " +
                        letters[currentIndexLetter + 1] + ";\n";

                ResultingComparison = stack.peek();
                solution += "Ответ: " + stack.peek().ToString();
                break;
            }
            currentIndexLetter++;
        }
        return solution;
    }


    public void AddComparisons(ComparisonByMod comparisonByMod) {
        _Comparisons.add(comparisonByMod);
    }

    private String forAllComparisonsreduceToOneCoefficientA() {
        String solution = "";
        for (int i = 0; i < _Comparisons.size(); i++) {
            solution += _Comparisons.get(i).reduceToOneCoefficientA() + "\n\n";
        }
        return solution;
    }
}

