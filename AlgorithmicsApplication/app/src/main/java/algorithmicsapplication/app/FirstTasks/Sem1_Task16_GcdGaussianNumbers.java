package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Pair;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 05/01/2015.
 */
public class Sem1_Task16_GcdGaussianNumbers extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task16);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task16_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Найти НОД в Z[i]:");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите число первое гаусово число ЧЕРЕЗ ЗАПЯТУЮ их вещественную и мниммую часть: ");
        layout.addView(enterNumberA);
        EditText inputBoxFirstNumberA = new EditText(this);
        inputBoxFirstNumberA.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumberA);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView enterNumberP = new TextView(this);
        enterNumberP.setText("Введите число второе гаусово число ЧЕРЕЗ ЗАПЯТУЮ их вещественную и мниммую часть: ");
        layout.addView(enterNumberP);
        EditText inputBoxSecondNumberB = new EditText(this);
        inputBoxSecondNumberB.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxSecondNumberB);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumberA, inputBoxSecondNumberB, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText secondNumberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, EditText secondNumberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.secondNumberInputBox = secondNumberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            GaussianNumbers firstGaussianNumber;
            GaussianNumbers secondGaussianNumber;
            try
            {
                String firstGaussianNumberString =  firstNumberInputBox.getText().toString();
                String secondGaussianNumberString =  secondNumberInputBox.getText().toString();
                String[] firstNumberComponents = firstGaussianNumberString.split(",");
                String[] secondNumberComponents = secondGaussianNumberString.split(",");
                firstGaussianNumber = new GaussianNumbers(Integer.parseInt(firstNumberComponents[0]),
                        Integer.parseInt(firstNumberComponents[1]));
                secondGaussianNumber = new GaussianNumbers(Integer.parseInt(secondNumberComponents[0]),
                        Integer.parseInt(secondNumberComponents[1]));
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                String solution = "Рассматривается деление гаусовых чисел: " + firstGaussianNumber.toString()
                        + " и " + secondGaussianNumber.toString() + ";\n\n\n";


                solution += "Норма (g) - определена следующим образом: g(a+b*i) = a^2 + b^2; \n";
                solution += "Умножение гаусовых чисел определено следующим образом: "
                        + "(a+b*i) * (c+d*i) = (a*c - b*d) + (a*d + b*c)*i;\n\n";

                GaussianNumbers dividend = null;
                GaussianNumbers divider = null;
                if (firstGaussianNumber.getNorm() > secondGaussianNumber.getNorm()) {
                    dividend = firstGaussianNumber;
                    divider = secondGaussianNumber;

                    solution += firstGaussianNumber.getNormInStringFormat() + " > " + secondGaussianNumber.getNormInStringFormat()
                            + ", поэтому делимое: " + dividend.toString() + ", делитель: " + divider + ";\n";
                } else {
                    dividend = secondGaussianNumber;
                    divider = firstGaussianNumber;
                    solution += firstGaussianNumber.getNormInStringFormat() + " < " + secondGaussianNumber.getNormInStringFormat()
                            + ", поэтому делимое: " + dividend.toString() + ", делитель: " + divider + ";\n";
                }

                while (!GaussianNumbers.IsDividedEvenly(dividend, divider)) {
                    Pair<ArrayList<GaussianNumbers>, String> pair = GaussianNumbers.Divide(dividend, divider);
                    ArrayList<GaussianNumbers> quotientAndRestNumber = pair.first;

                    solution += pair.second + ";\n";
                    solution += dividend.toString() + " = ( " + quotientAndRestNumber.get(0).toString() + " ) * ( " + divider.toString()
                            + " ) + ( " + quotientAndRestNumber.get(1).toString() + " );\n\n\n";


                    if (divider.getNorm() > quotientAndRestNumber.get(1).getNorm()) {
                        dividend = divider;
                        divider = quotientAndRestNumber.get(1);
                        solution += dividend.getNormInStringFormat() + " > " +  divider.getNormInStringFormat()
                                + ", поэтому\n\n Делимое: " + dividend.toString() + ", делитель: " + divider.toString() + ";\n";
                    } else {
                        dividend = quotientAndRestNumber.get(1);
                        divider = divider;
                        solution += dividend.getNormInStringFormat() + " < " + divider.getNormInStringFormat()
                                + ", поэтому\n\n Делимое: " +dividend.toString() + ", делитель: " + divider.toString()  + ";\n";
                    }
                    solution += "---------------------\n";
                }

                solution += GaussianNumbers.DivideByEvenly(dividend, divider);
                solution += ";\n\n\nНОД = " + divider;
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}