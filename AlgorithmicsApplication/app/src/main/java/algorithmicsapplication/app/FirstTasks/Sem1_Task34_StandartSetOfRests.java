package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 05/01/2015.
 */
public class Sem1_Task34_StandartSetOfRests extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task34);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task34_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Определить знак числа по стандартному набору остатков.");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterSourceVector = new TextView(this);
        enterSourceVector.setText("Введите вектор-X ЧЕРЕЗ ЗАПЯТЫЕ! ");
        layout.addView(enterSourceVector);
        EditText inputBaseNumberABox = new EditText(this);
        inputBaseNumberABox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberABox);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);


        TextView enterBaseVector = new TextView(this);
        enterBaseVector.setText("Введите вектор основания-B ЧЕРЕЗ ЗАПЯТЫЕ! ");
        layout.addView(enterBaseVector);
        EditText inputBaseNumberPBox = new EditText(this);
        inputBaseNumberPBox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberPBox);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBaseNumberABox, inputBaseNumberPBox, this));
    }


    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText inputBoxSourceVector;
        EditText inputBoxBaseVector;
        String solution = "";
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText inputBoxSourceVector, EditText inputBoxBaseVector, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.inputBoxSourceVector = inputBoxSourceVector;
            this.inputBoxBaseVector = inputBoxBaseVector;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

            ArrayList<Integer> xVector = new ArrayList<Integer>();
            ArrayList<Integer> bVector = new ArrayList<Integer>();
            int mN;
            try {
                String sourceVectorString = inputBoxSourceVector.getText().toString();
                String baseVectorString = inputBoxBaseVector.getText().toString();
                String[] sourceVectorComponent = sourceVectorString.split(",");
                String[] baseVectorComponent = baseVectorString.split(",");
                solution += "Исходный вектор x = {";
                for (int i = 0; i < sourceVectorComponent.length; i++) {
                    xVector.add(Integer.parseInt(sourceVectorComponent[i]));
                    solution += sourceVectorComponent[i];
                    if (i != sourceVectorComponent.length - 1)
                        solution += ", ";
                }
                solution += "}; \n Вектор основание beta = {";
                for (int i = 0; i < baseVectorComponent.length; i++) {
                    bVector.add(Integer.parseInt(baseVectorComponent[i]));
                    solution += baseVectorComponent[i];
                    if (i != sourceVectorComponent.length - 1)
                        solution += ", ";
                }
                solution += "} = {";
                for (int i = 0; i < baseVectorComponent.length; i++) {

                    solution += "m[" + (i+1) + "]";
                    if (i != sourceVectorComponent.length - 1)
                        solution += ", ";
                }
                solution += "};\n";
                solution += "m[i] - это i-ая компонента исходного вектора beta.\n\n";
                mN = bVector.get(bVector.size() - 1);
            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try {
                FindSign(xVector, bVector, mN, 1, "x");
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }

        private void FindSign(ArrayList<Integer> xVector, List<Integer> bVector, int mN, int iteration, String deductibleVectorName) {
            solution += "\n---------------\n";
            solution += "\nРассматриваем первую компоненту вектора-" + deductibleVectorName + ": ";
            int Qi = xVector.get(0);
            solution += "q[" + iteration + "] = " + Qi + ";\n";
            ArrayList<Integer> x1 = new ArrayList<Integer>();
            solution += "Вычитаем из каждой компоненты вектора-" + deductibleVectorName + " - первую компоненту этого вектора"
                    + ", равную: '" + Qi + "' (соотвественно отбрасывается первая нулевая компонента у получившегося вектора - и полагаем это  векторо-x["
                    + iteration + "], а также отбрасывается все компоненты вплоть до " + iteration
                    + "-ой компоненты у исходного вектора-beta и полагаем все это в качестве нового введенного вектор beta["
                    + iteration + "]:\n\n";
            solution += "x[" + iteration + "] = { ";
            for (int i = 1; i < xVector.size(); i++) {
                solution += "(" + xVector.get(i) + " - " + Qi + ")(mod " + bVector.get(i) + ")";
                if (i != xVector.size() - 1)
                    solution += ", ";
            }
            solution += "} = {";
            for (int i = 1; i < xVector.size(); i++) {
                int temp = PrimeFactorization.getPositiveNumberByMod((xVector.get(i) - Qi), bVector.get(i));
                x1.add(temp);
                solution += temp;
                if (i != xVector.size() - 1)
                    solution += ", ";
            }
            solution += "};\n ";
            ArrayList<Integer> bVector2 = new ArrayList<Integer>();
            solution += "beta[" + iteration + "] = {";
            for (int i = 1; i < bVector.size(); i++) {
                solution += bVector.get(i);
                if (i != bVector.size() - 1)
                    solution += ", ";

                bVector2.add(bVector.get(i));
            }
            solution += "};\n";
            solution += "Далее вычисляем вектор, получающийся как число, обратное к " + iteration +
                    "-ой компоненте исходного вектора beta - по модулю каждой из компонент вектора beta["
                    + iteration + "]:\n\n";
            ArrayList<Integer> m1InversedBy_B2 = new ArrayList<Integer>();
            solution += "(m[" + iteration + "])^(-1) (mod beta[" + (iteration + 1) + "]) = {";
            for (int i = 0; i < bVector2.size(); i++) {
                solution += bVector.get(0) + "^(-1)(mod" + bVector2.get(i) + ")";
                if (i != bVector2.size() - 1)
                    solution += ", ";
            }
            solution += "} = {";
            for (int i = 0; i < bVector2.size(); i++) {
                int currentInversed = PrimeFactorization.getReversedToNumberByMod(bVector.get(0), bVector2.get(i));
                m1InversedBy_B2.add(i, currentInversed);
                solution += currentInversed;
                if (i != bVector2.size() - 1)
                    solution += ", ";
            }
            solution += "};\n";
            solution += "Умножаем покомпонентно получившийся вектор: (m[" + iteration + ")^(-1)" +
                    " на вектор: х[" + iteration + "] - где каждая компонента берется по модулю каждой из" +
                    " компонент вектора beta[" + iteration + "]:\n\n";
            solution += "(m[" + iteration + "])^(-1) * x[" + iteration + "] = {";

            ArrayList<Integer> m1InversedX = new ArrayList<Integer>();
            for (int i = 0; i < bVector2.size(); i++) {
                solution += "(" + m1InversedBy_B2.get(i) + " * " + x1.get(i) + ") (mod " + bVector2.get(i) + ")";
                if (i != bVector2.size() - 1)
                    solution += ", ";
                int temp = PrimeFactorization.getPositiveNumberByMod(m1InversedBy_B2.get(i) * x1.get(i), bVector2.get(i));
            }
            solution += "} = {";
            for (int i = 0; i < bVector2.size(); i++) {
                int temp = PrimeFactorization.getPositiveNumberByMod(m1InversedBy_B2.get(i) * x1.get(i), bVector2.get(i));
                m1InversedX.add(i, temp);
                solution += temp;
                if (i != bVector2.size() - 1)
                    solution += ", ";
            }
            solution += "};\n";
            if (m1InversedX.size() > 1) {
                FindSign(m1InversedX, bVector2, mN, iteration + 1, "(m[" + (iteration) + "])^(-1) * x[" + iteration + "]");
            } else {
                solution += "Полагая, что m[n] -  значение последней компоненты исходного вектора beta - знак определяется следующим образом:\n";
                solution += "Если единственная оставшаяся компонента в векторе: (m[" + iteration
                        + "])^(-1) * x[" + iteration + "] < m[n]/2, то искомые знак - положительный.\n" +
                        "Если единственная оставшаяся компонента в векторе: (m[" + iteration
                        + "])^(-1) * x[" + iteration + "] >= m[n]/2, то искомые знак - отрицательый.";
                int qN = m1InversedX.get(0);
                double mNDividedBy2 = mN / 2;
                solution += "\nТ.к. q[n] = " + qN + "; m[n]/2 = " + mN + "/2 = " + mNDividedBy2 + ", то: \n";
                if (qN >= mNDividedBy2) {
                    solution += "(q[n] = " + qN + ") >= (m[n]/2 = " + mN + "/2 = " + mNDividedBy2 + ");\n";
                    solution += "Поэтому исходное число x - является ОТРИЦАТЕЛЬНО! \n";
                } else {
                    solution += "(q[n] = " + qN + ") < (m[n]/2 = " + mN + "/2 = " + mNDividedBy2 + ");\n";
                    solution += "Поэтому исходное число x - является ПОЛОЖИТЕЛЬНО! \n";
                }
            }
        }
    }
}