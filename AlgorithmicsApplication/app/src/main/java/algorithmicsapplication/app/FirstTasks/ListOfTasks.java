package algorithmicsapplication.app.FirstTasks;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 19/01/2015.
 */
public class ListOfTasks extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.list_of_tasks);

        LinearLayout layout = (LinearLayout) findViewById(R.id.list_of_tasks_layout);
        TextView taskTheory = new TextView(this);
        String listOfTasks = "4. Найти НОД десятичных чисел и представить в виде HOD = a * u + b * v\n";
        listOfTasks += "5. Найти НОД бинарных чисел\n";
        listOfTasks += "6.Найти все целочисленные решения уравнения ax + by = c\n";
        listOfTasks += "7. Разложить рациональное число в непрервыную цепную дробь" + "\n";
        listOfTasks += "8. Свернуть конечную цепную дробь" + "\n";
        listOfTasks += "9. Разложить в непрерывную дробь. Найти период" + "\n";
        listOfTasks += "10. Свернуть бесконечную периодическую дробь. Представить иррациональность в виде a+sqrt(b)/c" + "\n";
        listOfTasks += "11. Найти иррациональность alpha =[a1, .., an], если дана к-ая подходящая дробь и полное частное alpha[k]" + "\n";
        listOfTasks += "13. Оценить точность приближения числа alpha данной подходящей дробью" + "\n";
        listOfTasks += "14. Оценить близость к-ой подходяшей дроби к числу alpha" + "\n";
        listOfTasks += "15. Найти неполное частное и остаток от деления в кольце гауссовых чисел" + "\n";
        listOfTasks += "16. Найти НОД в кольце гаусовых чисел Z[i]" + "\n";
        listOfTasks += "17. Метод пробных делений - найти все простые числа" + "\n";
        listOfTasks += "18. Используя расширенный алгоритм Евклида решисть сравнение" + "\n";
        listOfTasks += "19. Используя расширенный алгоритм Евклида решисть сравнение. Найти все решения по данному модулю" + "\n";
        listOfTasks += "20. С помщью расширенного алгоритма Евклида найти обратный в кольце Zm" + "\n";
        listOfTasks += "21. Решить систему сравнений используя метод последовательного решения пары сравнений" + "\n";
        listOfTasks += "22. Решить систему сравнений используя формулу x = Sum(a[i] * M[i] * N[i] modM)" + "\n";
        listOfTasks += "23. Вычислить значение фукнции ЭЙлера " + "\n";
        listOfTasks += "24. ВЫчислить значений функции Мебиуса" + "\n";
        listOfTasks += "25. Найти обратный используя Теорму Ферма" + "\n";
        listOfTasks += "26. Найти обратный используя теорему Эйлера" + "\n";
        listOfTasks += "27. Найти минимальнон число умножений для возведение в степень a^n" + "\n";
        listOfTasks += "28. Найти минимальное число умножений для вычисления произведения A * B" + "\n";
        listOfTasks += "29. Найти примитивный корень по модулю (один, в отличие от 30-ой задачи)" + "\n";
        listOfTasks += "30. Найти ВСЕ примитивные корни по модулю" + "\n";
        listOfTasks += "31. Является ли мультипликативная группа циклической. Найти порядок группы. Образующую." + "\n";
        listOfTasks += "32. Описать стркутуру мультипликативной группы . Найти порядок группы. Найти элемент макс. порядка." + "\n";
        listOfTasks += "33. Определить является ли число псевдопростым, сильнопсевдопростым." + "\n";
        listOfTasks += "34. Число х представлено стандартным набором остатков относительно вектора оснований. Определить знак числа." + "\n";
       taskTheory.setText(listOfTasks);
        layout.addView(taskTheory);


    }
}
