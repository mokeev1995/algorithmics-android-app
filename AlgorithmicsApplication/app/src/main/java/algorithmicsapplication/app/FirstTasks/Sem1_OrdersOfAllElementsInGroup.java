package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 08/01/2015.
 */
public class Sem1_OrdersOfAllElementsInGroup extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_allordersofgroup);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_allordersofgroup_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Найти порядок для каждого элемента группы");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("\n-----------------\nДопускает ДЛИТЕЛЬНАЯ ЗАДЕРЖКА во время вычислений!!!\n" +
                "-----------------\n");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите число n: ");
        layout.addView(enterNumberA);
        EditText inputBaseNumberABox = new EditText(this);
        inputBaseNumberABox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberABox);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBaseNumberABox, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText numberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText numberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.numberInputBox = numberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

            int inputNumber;
            try {
                inputNumber = Integer.parseInt(numberInputBox.getText().toString());
            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try {
                String solution = "Рассматривается группа Z" + inputNumber + ";\n";
                int orderSourceGroup = PrimeFactorization.eulerFunction(inputNumber);
                solution += "Порядок группы равен: '" + orderSourceGroup + "';\n";
                for (int i = 2; i < inputNumber; i++) {
                    int testRest = i;
                    for (int j = 2; j < orderSourceGroup; j++) {
                        testRest = PrimeFactorization.raiseToPowByMod(i, j, inputNumber);
                        solution += i + "^" + j + "(mod " + inputNumber + ") = " + testRest
                                + ";\n";
                        if (testRest == 1) {
                            solution += "Порядок элемента '" + i + "' - равен: '" + j + "'\n;";
                            break;
                        }
                    }
                }
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);

            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}
