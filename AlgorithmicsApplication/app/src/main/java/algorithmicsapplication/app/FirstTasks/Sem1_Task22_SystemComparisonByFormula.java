package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 07/01/2015.
 */
public class Sem1_Task22_SystemComparisonByFormula extends ActionBarActivity {

    ArrayList<EditText> editTextFields = new ArrayList<EditText>();

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task22);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task22_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Решить системы сравнений c помощью формулы x= Sum(ai * Mi * Ni)(mod M)");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("Сгенереуйте необходимое количество полей и введите ЧЕРЕЗ ЗАПЯТЫЕ 3 числа (a, b, m), сравнения виде a * x = b(mod m)");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        Button addNewFieldButton = new Button(this);
        addNewFieldButton.setText("Добавить дополнительное поле");
        layout.addView(addNewFieldButton);
        addNewFieldButton.setOnClickListener(new AddNewFieldsListener(layout, this));

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new ExecutionListener(layout, resultTextView));
    }

    public class ExecutionListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        Context context;

        public ExecutionListener(LinearLayout linearLayout, TextView resultTextView) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            SystemComparisons systemComparisons = new SystemComparisons();
            String solution = "Введена следующая система сравнений: \n";
            int multipliedModM = 1;
            try
            {
                for(int i = 0; i < editTextFields.size(); i++)
                {
                    String inputString = editTextFields.get(i).getText().toString();
                    String[] components = inputString.split(",");
                    systemComparisons.AddComparisons(new ComparisonByMod( Integer.parseInt(components[0]),
                            Integer.parseInt(components[1]), Integer.parseInt(components[2])));
                    solution += "(" + Integer.parseInt(components[0]) + ") * x = (" + Integer.parseInt(components[1])
                            + ")(mod " + Integer.parseInt(components[2]) + ")\n";
                }
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                for(int i = 0; i < systemComparisons._Comparisons.size(); i++)
                {
                    multipliedModM = multipliedModM * systemComparisons._Comparisons.get(i).modValue;
                }
                ArrayList<Integer> Mi_list = new ArrayList<Integer>();
                for(int i = 0; i < systemComparisons._Comparisons.size(); i++)
                {
                    Mi_list.add(multipliedModM / systemComparisons._Comparisons.get(i).modValue);
                }

                ArrayList<Integer> Ni_list = new ArrayList<Integer>();
                for(int i = 0; i < Mi_list.size(); i++)
                {
                    solution += "Найдем коэффициенты Безу для чисел: '" + (multipliedModM / Mi_list.get(i)) + "' и '"
                            + Mi_list.get(i) + "':\n";
                    List<AlgorithmTableRow> rows = new ArrayList<AlgorithmTableRow>();
                    TableGenerator tableGenerator = new TableGenerator();
                    tableGenerator.tableGeneration(multipliedModM / Mi_list.get(i), Mi_list.get(i), 0, rows); // ИЗМЕНЯЕТ List<AlgorithmTableRow> rows!!!
                    linearLayout.addView(GetTableLayout(rows));

                    AlgorithmTableRow lastRow = rows.get(rows.size()-1);
                    Ni_list.add(lastRow.uI);
                }
                solution += "В итоге получаем: \n x = ";
                int totalSum = 0;
                int totalMod = 1;
                for(int i = 0; i < Mi_list.size(); i++)
                {
                    solution += "(" + Ni_list.get(i) + ") * ("
                            + Mi_list.get(i) + ") * (" +  systemComparisons._Comparisons.get(i).bCoefficient + ") +";
                    totalSum += Ni_list.get(i) * Mi_list.get(i) * systemComparisons._Comparisons.get(i).bCoefficient;
                    totalMod *= systemComparisons._Comparisons.get(i).modValue;
                }
                solution += " = (" + totalSum + ")(mod (" + totalMod + ") ) = ";
                solution += "(" + PrimeFactorization.getPositiveNumberByMod(totalSum, totalMod) + ")(mod " + totalMod + ");\n";

                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }

    private LinearLayout generateTableCell(String text)
    {
        LinearLayout cell = new LinearLayout(this);
        TextView textView = new TextView(this);
        textView.setText(text);
        textView.setPadding(0, 0, 4, 3);
        cell.addView(textView);
        cell.setBackgroundColor(Color.GRAY);
        return cell;
    }

    private TableLayout GetTableLayout(List<AlgorithmTableRow> rows)
    {
        TableLayout table = new TableLayout(this);
        TableRow header = new TableRow(this);
        header.addView(generateTableCell("i"));
        header.addView(generateTableCell("q"));
        header.addView(generateTableCell("u[i]"));
        header.addView(generateTableCell("v[i]"));
        header.addView(generateTableCell("r[i]"));
        header.addView(generateTableCell("u[i+1]"));
        header.addView(generateTableCell("v[i+1]"));
        header.addView(generateTableCell("r[i+1]"));
        table.addView(header);
        for(int i = 0; i < rows.size(); i++)
        {
            TableRow tr = new TableRow(this);
            AlgorithmTableRow currentRow = rows.get(i);
            tr.addView(generateTableCell(Integer.toString(i)));
            tr.addView(generateTableCell(Integer.toString(currentRow.q)));
            tr.addView(generateTableCell(Integer.toString(currentRow.uI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.vI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.rI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.uIplus1)));
            tr.addView(generateTableCell(Integer.toString(currentRow.vIplus1)));
            tr.addView(generateTableCell(Integer.toString(currentRow.rIplus1)));
            table.addView(tr);
        }
        TableRow.LayoutParams param = new TableRow.LayoutParams();
        param.setMargins(0, 0, 0, 1);
        table.setLayoutParams(param);
        return table;
    }

    public class AddNewFieldsListener implements View.OnClickListener {
        LinearLayout linearLayout;
        Context context;

        public AddNewFieldsListener(LinearLayout linearLayout, Context context) {
            this.linearLayout = linearLayout;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            EditText newEditTextView = new EditText(context);
            newEditTextView.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
            editTextFields.add(newEditTextView);
            linearLayout.addView(newEditTextView);
        }
    }
}