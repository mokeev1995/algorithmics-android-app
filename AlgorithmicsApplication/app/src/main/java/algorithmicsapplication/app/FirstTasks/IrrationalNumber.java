package algorithmicsapplication.app.FirstTasks;

public class IrrationalNumber {
    int a;
    int d;
    int b;
    int c;
    int integerPart;
    int iteration;
    IrrationalNumber FractionalPart;
    IrrationalNumber reversedFractionalPartForAssignment;

    String solution = "";

    public static IrrationalNumber add(IrrationalNumber firstIrrationalNumber, Integer secondIntegerNumber) {
        Integer newNumberA = (secondIntegerNumber * firstIrrationalNumber.c) + firstIrrationalNumber.a;
        Integer newNumberD = firstIrrationalNumber.d;
        Integer newNumberB = firstIrrationalNumber.b;
        Integer newNumberC = firstIrrationalNumber.c;

        IrrationalNumber temp = new IrrationalNumber(newNumberA, newNumberD, newNumberB, newNumberC, 0);
        temp.simplify();
        return temp;
    }

    public static IrrationalNumber multiply(IrrationalNumber firstIrrationalNumber, Integer secondIntegerNumber) {
        Integer newNumberA = secondIntegerNumber * firstIrrationalNumber.a;
        Integer newNumberD = secondIntegerNumber * firstIrrationalNumber.d;
        Integer newNumberB = firstIrrationalNumber.b;
        Integer newNumberC = firstIrrationalNumber.c * 1;

        IrrationalNumber temp = new IrrationalNumber(newNumberA, newNumberD, newNumberB, newNumberC, 0);
        temp.simplify();
        return temp;
    }

    public IrrationalNumber(int a, int d, int b, int c, int iteration) {
        this.a = a;
        this.d = d;
        this.b = b;
        this.c = c;
        this.iteration = iteration;
    }

    public IrrationalNumber() {
    }

    public void compute() {
        caclulateIntegerPart();
        substractFromCurrentIrrationalNumberIntegerPart();
        reversedFractionalPartForAssignment = reverseIrrationalNumber(FractionalPart);
        reversedFractionalPartForAssignment.iteration = reversedFractionalPartForAssignment.iteration + 1;
    }

    public void simplify() {
        int multiplierUnderSqrt = 1;
        for (int i = 2; i <= b; i++) {
            if (i * i > b)
                break;
            if (b % (i * i) == 0) {
                multiplierUnderSqrt = i;
            }
        }

        d = d * multiplierUnderSqrt;
        b = b / (multiplierUnderSqrt * multiplierUnderSqrt);


        int positiveD = d < 0 ? -(d)
                : d;
        int positiveC = c < 0 ? -(c) : c;
        int positiveA = a < 0 ? -(a) : a;

        if (egcd(positiveD, positiveC) != 1 && egcd(positiveD, positiveC) != 0
                && egcd(positiveA, positiveC) != 1 && egcd(positiveA, positiveC) != 0) {
            int firstEgcd = egcd(positiveD, positiveC);
            int secondEgcd = egcd(positiveA, positiveC);
            int totalEgcd = egcd(firstEgcd, secondEgcd);
            d = d / totalEgcd;
            c = c / totalEgcd;
            a = a / totalEgcd;
        }
    }

    public void caclulateIntegerPart() {
        solution += "\nКвадратные скобочки - целая часть от числа; Фигурные скобки - дробная часть числа.\n\n";
        solution += printIrrationalNumber() + " = [ " + printIrrationalNumber()
                + " ] + { " + printIrrationalNumber() + " } = ";

        double firstMultiplier = ((double) d) * Math.sqrt(b);
        integerPart = (int) ((((double) a) + firstMultiplier) / ((double) c));
        solution += integerPart + " + ";
    }

    public void substractFromCurrentIrrationalNumberIntegerPart() {
        IrrationalNumber tempIrrationalNumber = new IrrationalNumber();
        tempIrrationalNumber.a = a - (integerPart * c);
        tempIrrationalNumber.d = d;
        tempIrrationalNumber.b = b;
        tempIrrationalNumber.c = c;
        FractionalPart = tempIrrationalNumber;
        solution += "(" + FractionalPart.printIrrationalNumber() + ") = a[" + iteration + "] + ( 1/ alpha[" + iteration
                + "] );\n\n";
    }

    public int egcd(int first, int second) {
        if (first == 0)
            return second;

        while (second != 0) {
            if (first > second)
                first = first - second;
            else
                second = second - first;
        }

        return first;
    }

    public IrrationalNumber reverseIrrationalNumber(IrrationalNumber irrationalNumber) {
        IrrationalNumber reversedIrrationalNumber = new IrrationalNumber();
        solution += "(1 / alpha[" + iteration + "]) = " + irrationalNumber.printIrrationalNumber() + ", \n\n=>"
                + " alpha[" + iteration + "] = 1/(" + irrationalNumber.printIrrationalNumber() + ") = ";
        solution += "(" + irrationalNumber.c + ") * (" + irrationalNumber.d + " * sqrt("
                + irrationalNumber.b + ") ) / (" + irrationalNumber.b + " - (" + irrationalNumber.b + ") * ("
                + irrationalNumber.b + ") ) = ";

        reversedIrrationalNumber.a = (irrationalNumber.a * irrationalNumber.c);
        reversedIrrationalNumber.d = -(irrationalNumber.d * irrationalNumber.c);
        reversedIrrationalNumber.b = irrationalNumber.b;
        reversedIrrationalNumber.c = (irrationalNumber.a * irrationalNumber.a)
                - (irrationalNumber.d * irrationalNumber.d * irrationalNumber.b);
        solution += reversedIrrationalNumber.printIrrationalNumber();

        if(reversedIrrationalNumber.c < 0 && reversedIrrationalNumber.a < 0 && reversedIrrationalNumber.d <0)
        {
            reversedIrrationalNumber.c = - reversedIrrationalNumber.c;
            reversedIrrationalNumber.a = - reversedIrrationalNumber.a;
            reversedIrrationalNumber.d = - reversedIrrationalNumber.d;
        }
        int positiveD = reversedIrrationalNumber.d < 0 ? -(reversedIrrationalNumber.d)
                : reversedIrrationalNumber.d;
        int positiveC = reversedIrrationalNumber.c < 0 ? -(reversedIrrationalNumber.c) : reversedIrrationalNumber.c;
        int positiveA = reversedIrrationalNumber.a < 0 ? -(reversedIrrationalNumber.a) : reversedIrrationalNumber.a;

        if (egcd(positiveD, positiveC) != 1 && egcd(positiveD, positiveC) != 0
                && egcd(positiveA, positiveC) != 1 && egcd(positiveA, positiveC) != 0) {
            int firstEgcd = egcd(positiveD, positiveC);
            int secondEgcd = egcd(positiveA, positiveC);
            int totalEgcd = egcd(firstEgcd, secondEgcd);
            reversedIrrationalNumber.d = reversedIrrationalNumber.d / totalEgcd;
            reversedIrrationalNumber.c = reversedIrrationalNumber.c / totalEgcd;
            reversedIrrationalNumber.a = reversedIrrationalNumber.a / totalEgcd;
        }
        solution += " = " + reversedIrrationalNumber.printIrrationalNumber();
        solution += ".\n";

        return reversedIrrationalNumber;
    }

    public boolean equals(IrrationalNumber irrationalNumber) {
        if (a == irrationalNumber.a && b == irrationalNumber.b && d == irrationalNumber.d
                && c == irrationalNumber.c)
            return true;
        else
            return false;
    }

    public String printIrrationalNumber() {
        String temp = "( (" + a + ") + (" + d + ") * sqrt(" + b + ") ) / ("
                + c + ")";
        return temp;
    }
}
