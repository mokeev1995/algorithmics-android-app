package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 04/01/2015.
 */
public class Sem1_Task27_MinMultiplications extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task28);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task28_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Найти минимально число УМНОЖЕНИЯ для вычисления a^n");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите первое число: ");
        layout.addView(enterNumberA);
        EditText inputBaseNumberABox = new EditText(this);
        inputBaseNumberABox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberABox);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBaseNumberABox, this));
    }

    public class CustomClickListener implements View.OnClickListener
    {
        LinearLayout layout;
        TextView resultTextView;
        EditText numberInputBox;
        Context context;

        public CustomClickListener(LinearLayout layout, TextView resultTextView, EditText numberInputBox, Context context) {
            this.layout = layout;
            this.resultTextView = resultTextView;
            this.numberInputBox = numberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            int number;
            try
            {
                number = Integer.parseInt(numberInputBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                String binaryNumber = Integer.toBinaryString(number);
                String solution = "Число '" + number + "' в двоичном формате: " + binaryNumber + "; \n";
                solution += "a[0] = a" + "; \n";
                for(int i = 1; i < binaryNumber.length(); i++)
                {
                    solution += "a[" + i + "] = " + "a^" + PrimeFactorization.raiseToPow(2,i) + "= (a["+ (i-1) + "])^2" + "; \n";
                }
                int amountDigits = 0;
                for(int i = 0; i < binaryNumber.length(); i++)
                {
                    boolean isOne = false;
                    if(binaryNumber.charAt(i) == '1')
                    {
                        isOne = true;
                        solution += "a[" + (binaryNumber.length()-i-1) + "]";
                        amountDigits++;
                    }
                    if(i != binaryNumber.length() - 1 && isOne)
                        solution += " * ";
                }

                solution += " = ";
                for(int i = 0; i < binaryNumber.length(); i++)
                {
                    boolean isOne = false;
                    if(binaryNumber.charAt(i) == '1')
                    {
                        isOne = true;
                        solution += "a^" + PrimeFactorization.raiseToPow(2, (binaryNumber.length()-i-1));
                    }
                    if(i != binaryNumber.length() - 1 && isOne)
                        solution += " * ";
                }

                solution += "= a^" + number + "\n";
                solution += "Всего потребовалось: " + (binaryNumber.length() - 1) + " + " + (amountDigits -1)
                         + " = "+ (binaryNumber.length() - 1 + (amountDigits -1))+" умножений";
                resultTextView.setText(solution);

                layout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}
