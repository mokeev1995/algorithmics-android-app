package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 04/01/2015.
 */
public class Sem1_Task26_EulerTheorem extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task26);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task26_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Используя теорему ЭЙЛЕРА найти обратный элемент a^(-1) в кольце Zp");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("Если p - простое, то a^(-1) = a^(F(n)-1), где F(n) - функий Эйлера");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите число а: ");
        layout.addView(enterNumberA);
        EditText inputBaseNumberABox = new EditText(this);
        inputBaseNumberABox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberABox);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        TextView enterNumberP = new TextView(this);
        enterNumberP.setText("Введите число p: ");
        layout.addView(enterNumberP);
        EditText inputBaseNumberPBox = new EditText(this);
        inputBaseNumberPBox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberPBox);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBaseNumberABox, inputBaseNumberPBox, this));
    }

    public class CustomClickListener implements View.OnClickListener
    {
        LinearLayout layout;
        TextView resultTextView;
        EditText inputNumberABox;
        EditText inputNumberPBox;
        Context context;

        public CustomClickListener(LinearLayout layout, TextView resultTextView, EditText inputNumberABox, EditText inputNumberPBox, Context context) {
            this.layout = layout;
            this.resultTextView = resultTextView;
            this.inputNumberABox = inputNumberABox;
            this.inputNumberPBox = inputNumberPBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);
            String solution = "";

            int inputNumberA;
            int inputNumberP;
            try
            {
                inputNumberA = Integer.parseInt(inputNumberABox.getText().toString());
                inputNumberP = Integer.parseInt(inputNumberPBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                if(PrimeFactorization.isCoprime(inputNumberP, inputNumberA))
                {
                    solution += inputNumberP + " и " + inputNumberA + " - взаимопросты, поэтому можно воспользоваться формулой:"
                            + ", что a^(-1) = a^(F(p)-1) в Zp, где F(p) - функция Эйлера\n";
                    int eulerFunctionValue = PrimeFactorization.eulerFunction(inputNumberP);
                    solution += "F(" + inputNumberP + ") = " + eulerFunctionValue + "\n\n";
                    int resultingNumber = inputNumberA;
                    for(int i = 2; i <= eulerFunctionValue - 1; i++)
                    {
                        solution += inputNumberA + "^" + i + " = " + (resultingNumber * inputNumberA);
                        resultingNumber = (resultingNumber * inputNumberA) % inputNumberP;
                        solution += " = " + resultingNumber + "(mod " + inputNumberP + ").\n";
                    }
                    solution += inputNumberA + "^ (-1) =" + inputNumberA + "^" + (eulerFunctionValue-1) + " = " + resultingNumber;
                    resultTextView.setText(solution);
                }
                else
                    resultTextView.setText("По теореме Эйлера числа a и p - должны быть взаимнопростыми!");

                layout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}
