package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 09/01/2015.
 */
public class Sem1_Task11_FindIrrationalNumberByFractionAndQuotaiont extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task11);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task11_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Найти иррациональность по подходящей цепной дроби и полному иррациональному частному");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("\n\n----------\n\nВНИМАНИЕ: вводить коэффициенты для конкретно вашего числа в виде: (a + d * sqrt(b))/b " +
                "- т.к. этого требует универсальность алгоритма для входных данных!\n\n------------\n");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNominator = new TextView(this);
        enterNominator.setText("Введите числитель");
        layout.addView(enterNominator);
        EditText inputBoxNominator = new EditText(this);
        inputBoxNominator.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxNominator);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView enterDenominator = new TextView(this);
        enterDenominator.setText("Введите знаменатель");
        layout.addView(enterDenominator);
        EditText inputBoxDenominator = new EditText(this);
        inputBoxDenominator.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxDenominator);

        TextView textViewEnterFirstNumber = new TextView(this);
        textViewEnterFirstNumber.setText("Введите коэффициент a:");
        layout.addView(textViewEnterFirstNumber);
        EditText inputBoxFirstNumber = new EditText(this);
        inputBoxFirstNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumber);

        TextView textViewEnterSecondNumber = new TextView(this);
        textViewEnterSecondNumber.setText("Введите коэффициент d:");
        layout.addView(textViewEnterSecondNumber);
        EditText inputBoxSecondNumber = new EditText(this);
        inputBoxSecondNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxSecondNumber);

        TextView textViewEnterThirdNumber = new TextView(this);
        textViewEnterThirdNumber.setText("Введите коэффициент b:");
        layout.addView(textViewEnterThirdNumber);
        EditText inputBoxThirdNumber = new EditText(this);
        inputBoxThirdNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxThirdNumber);

        TextView textViewEnterFourthNumber = new TextView(this);
        textViewEnterFourthNumber.setText("Введите коэффициент d:");
        layout.addView(textViewEnterFourthNumber);
        EditText inputBoFourthNumber = new EditText(this);
        inputBoFourthNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoFourthNumber);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumber, inputBoxSecondNumber, inputBoxThirdNumber, inputBoFourthNumber, inputBoxNominator, inputBoxDenominator, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText secondNumberInputBox;
        EditText thirdNumberInputBox;
        EditText fourthNumberInputBox;
        EditText inputBoxNominator;
        EditText inputBoxDenominator;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, EditText secondNumberInputBox, EditText thirdNumberInputBox, EditText fourthNumberInputBox, EditText inputBoxNominator, EditText inputBoxDenominator, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.secondNumberInputBox = secondNumberInputBox;
            this.thirdNumberInputBox = thirdNumberInputBox;
            this.fourthNumberInputBox = fourthNumberInputBox;
            this.inputBoxNominator = inputBoxNominator;
            this.inputBoxDenominator = inputBoxDenominator;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            int coefficientA;
            int coefficientD;
            int coefficientB;
            int coefficientC;
            int numerator;
            int denominator;
            try
            {
                coefficientA = Integer.parseInt(firstNumberInputBox.getText().toString());
                coefficientD = Integer.parseInt(secondNumberInputBox.getText().toString());
                coefficientB = Integer.parseInt(thirdNumberInputBox.getText().toString());
                coefficientC = Integer.parseInt(fourthNumberInputBox.getText().toString());
                numerator  = Integer.parseInt(inputBoxNominator.getText().toString());
                denominator = Integer.parseInt(inputBoxDenominator.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                IrrationalNumber irrationalNumber = new IrrationalNumber(coefficientA, coefficientD, coefficientB, coefficientC, 0);

                String solution = "Рассматривается дробь: (" + numerator + ") / (" + denominator + ");\n";

                int currentQuotient = 0;
                int rest = 0;
                if(numerator < 0 || denominator < 0)
                {
                    currentQuotient = (numerator / denominator) -1;
                    rest = numerator - (currentQuotient * denominator);
                    solution += "Т.е. у нас дробь отрицательно, то необходимо добиться, чтобы q1 < 0 & q[i] > 0, где i=2..n \n";
                }
                else
                {
                    currentQuotient = numerator / denominator;
                    rest = numerator % denominator;
                }
                ArrayList<Integer> quotientList = new ArrayList<Integer>();
                quotientList.add(currentQuotient);
                while(true)
                {
                    solution += "(" + numerator + ") = (" + currentQuotient + ") * (" + denominator + ") + (" + rest + ");\n";
                    numerator = denominator;
                    denominator = rest;
                    currentQuotient = numerator / denominator;
                    rest = numerator % denominator;
                    quotientList.add(currentQuotient);
                    if(rest == 0)
                    {
                        solution += "(" + numerator + ") = (" + currentQuotient + ") * (" + denominator + ");\n";
                        break;
                    }
                }

                solution += "Исходная дробь в виде цепной дроби: [";
                for(int i = 0; i < quotientList.size(); i++)
                {
                    solution += quotientList.get(i);
                    if(i != quotientList.size()-1)
                        solution += ",";
                }
                solution += "]\n\n";
                solution += calculateIrrationalNumberByContinuedFraction(irrationalNumber, quotientList);
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }

        public String calculateIrrationalNumberByContinuedFraction(IrrationalNumber irrationalNumber, ArrayList<Integer> quotientList)
        {
            ArrayList<Integer> qList = quotientList;
            String solution = "";
            ArrayList<Integer> pList = new ArrayList<Integer>();
            ArrayList<Integer> QList = new ArrayList<Integer>();
            pList.add(1);
            pList.add(qList.get(0));
            QList.add(0);
            QList.add(1);
            for(int i = 2; i <= qList.size()+1; i++)
            {
                if(i == qList.size()+1)
                {
                    IrrationalNumber pnIrrational = IrrationalNumber.multiply(irrationalNumber, pList.get(i-1));
                    pnIrrational = IrrationalNumber.add(pnIrrational, pList.get(i-2));
                    solution += "P[" + i + "] = q[" + i + "] * P[" + (i-1) + "] + P[" + (i-2) + "] = ("
                            + irrationalNumber.printIrrationalNumber() +") * (" + pList.get(i-1) + ") + (" + pList.get(i-2) + ") = (" + pnIrrational.printIrrationalNumber()     +");\n";
                    IrrationalNumber qnIrrational = IrrationalNumber.multiply(irrationalNumber, QList.get(i-1));
                    qnIrrational = IrrationalNumber.add(qnIrrational, QList.get(i-2));
                    solution += "Q[" + i + "] = q[" + i + "] * Q[" + (i-1) + "] + Q[" + (i-2) + "] = ("
                            + irrationalNumber.printIrrationalNumber() +") * (" + QList.get(i-1) + ") + (" + QList.get(i-2) + ") = (" + qnIrrational.printIrrationalNumber() +");\n";

                    solution += "\nОТВЕТ: " + pnIrrational.printIrrationalNumber() + "/ " + qnIrrational.printIrrationalNumber();
                    solution += "\n\nНЕ ЗАБУДЬТЕ ПОЛУЧИВЩУЮСЯ ИРРАЦИОНАЛЬНУЮ ДРОБЬ САМОСТОЯТЕЛЬНО СОКРАТИТЬ, ЕСЛИ ПОЛУЧИТСЯ!";
                    break;
                }
                int currentP = qList.get(i-1) * pList.get(i-1) + pList.get(i-2);
                pList.add(currentP);
                solution += "P[" + i + "] = q[" + i + "] * P[" + (i-1) + "] + P[" + (i-2) + "] = ("
                        + qList.get(i-1) +") * (" + pList.get(i-1) + ") + (" + pList.get(i-2) + ") = (" + currentP +");\n";
                int currentQ = qList.get(i-1) * QList.get(i-1) + QList.get(i-2);
                QList.add(currentQ);
                solution += "Q[" + i + "] = q[" + i + "] * Q[" + (i-1) + "] + Q[" + (i-2) + "] = ("
                        + qList.get(i-1) +") * (" + QList.get(i-1) + ") + (" + QList.get(i-2) + ") = (" + currentQ +");\n";

            }
            return solution;
        }
    }
}
