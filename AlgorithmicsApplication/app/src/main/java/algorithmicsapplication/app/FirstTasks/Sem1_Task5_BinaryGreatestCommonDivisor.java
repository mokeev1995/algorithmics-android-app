package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 05/01/2015.
 */
public class Sem1_Task5_BinaryGreatestCommonDivisor extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task5);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task5_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Найти НОД двух БИНАРНЫХ чисел:");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите число a - в двоичном виде:");
        layout.addView(enterNumberA);
        EditText inputBoxFirstNumberA = new EditText(this);
        inputBoxFirstNumberA.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumberA);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView enterNumberP = new TextView(this);
        enterNumberP.setText("Введите число b - в двоичном виде:");
        layout.addView(enterNumberP);
        EditText inputBoxSecondNumberB = new EditText(this);
        inputBoxSecondNumberB.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxSecondNumberB);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumberA, inputBoxSecondNumberB, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText secondNumberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, EditText secondNumberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.secondNumberInputBox = secondNumberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            String firstBinaryNumber = firstNumberInputBox.getText().toString();
            String secondBinaryNumber = secondNumberInputBox.getText().toString();

            try
            {
                Integer.parseInt(firstBinaryNumber, 2);
                Integer.parseInt(secondBinaryNumber, 2);
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }



            try
            {
                int firstNumberZeros = 0;
                for(int i = firstBinaryNumber.length() -1; i >= 0; i--)
                {
                    if(firstBinaryNumber.charAt(i) == '1')
                        break;
                    else
                        firstNumberZeros++;
                }

                int secondNumberZeros = 0;//
                for(int i = secondBinaryNumber.length() -1; i >= 0; i--)
                {
                    if(secondBinaryNumber.charAt(i) == '1')
                        break;
                    else
                        secondNumberZeros++;
                }
                int kCommonDegree = Math.min(firstNumberZeros, secondNumberZeros);
                String solution = "d = НОД(a, b) = НОД(" + firstBinaryNumber + " , " + secondBinaryNumber + ");\n";
                String firstBinaryDividedByPowOfTwo = firstBinaryNumber.substring(0, firstBinaryNumber.length()-kCommonDegree);
                String secondBinaryDividedByPowOfTwo = secondBinaryNumber.substring(0, secondBinaryNumber.length()-kCommonDegree);
                int firstNumberDividedByPowOfTwo = Integer.parseInt(firstBinaryDividedByPowOfTwo, 2);
                int secondNumberDividedByPowOfTwo = Integer.parseInt(secondBinaryDividedByPowOfTwo, 2);
                solution += "d = НОД(a, b) = 2^" + kCommonDegree + " * d1 = " + Integer.toBinaryString(PrimeFactorization.raiseToPow(2, kCommonDegree))
                        + " * HOD(" + firstBinaryDividedByPowOfTwo + " , " + secondBinaryDividedByPowOfTwo +
                        ", где d1 - равен:"+ "\n";
                while(firstNumberDividedByPowOfTwo != secondNumberDividedByPowOfTwo)
                {
                    if(firstNumberDividedByPowOfTwo % 2 == 0)
                    {
                        int bitsOffset = 0;
                        int temp = firstNumberDividedByPowOfTwo;
                        while(firstNumberDividedByPowOfTwo % 2 == 0)
                        {
                            bitsOffset++;
                            firstNumberDividedByPowOfTwo /= 2;
                        }
                        solution += "d1 = НОД(" + Integer.toBinaryString(temp) + " , " + Integer.toBinaryString(secondNumberDividedByPowOfTwo)
                                + ") = " + "НОД(" +Integer.toBinaryString(firstNumberDividedByPowOfTwo) + " , " +
                                Integer.toBinaryString(secondNumberDividedByPowOfTwo) + ") - сдвинули первое ЧЕТНОЕ число на " + bitsOffset
                                + " разрядов;" + "\n";

                    }
                    if(secondNumberDividedByPowOfTwo % 2 == 0)
                    {
                        int bitsOffset = 0;
                        int temp = secondNumberDividedByPowOfTwo;
                        while(secondNumberDividedByPowOfTwo % 2 == 0)
                        {
                            bitsOffset++;
                            secondNumberDividedByPowOfTwo /= 2;
                        }

                        solution += "d1 = НОД(" +Integer.toBinaryString(firstNumberDividedByPowOfTwo)  + " , " +  Integer.toBinaryString(temp)
                                + ") = " + "НОД(" +Integer.toBinaryString(firstNumberDividedByPowOfTwo) + " , " +
                                Integer.toBinaryString(secondNumberDividedByPowOfTwo) + ") - сдвинули второе ЧЕТНОЕ число на " + bitsOffset
                                + " разрядов;" + "\n";
                    }

                    if(firstNumberDividedByPowOfTwo > secondNumberDividedByPowOfTwo)
                    {
                        int temp = firstNumberDividedByPowOfTwo;
                        firstNumberDividedByPowOfTwo -= secondNumberDividedByPowOfTwo;
                        solution += "d1 = НОД(" + Integer.toBinaryString(temp) + " - " + Integer.toBinaryString(secondNumberDividedByPowOfTwo)
                                + " , " +  Integer.toBinaryString(secondNumberDividedByPowOfTwo)
                                + ") = " + "НОД(" + Integer.toBinaryString(firstNumberDividedByPowOfTwo) + " , " +
                                Integer.toBinaryString(secondNumberDividedByPowOfTwo) + ") - вычли из первого - второе \n";
                    }
                    else if(firstNumberDividedByPowOfTwo < secondNumberDividedByPowOfTwo)
                    {
                        int temp = secondNumberDividedByPowOfTwo;
                        secondNumberDividedByPowOfTwo -= firstNumberDividedByPowOfTwo;
                        solution += "d1 = НОД(" + Integer.toBinaryString(firstNumberDividedByPowOfTwo)
                                + " , " +  Integer.toBinaryString(temp) + " - " + Integer.toBinaryString(firstNumberDividedByPowOfTwo)
                                + ") = " + "НОД(" + Integer.toBinaryString(firstNumberDividedByPowOfTwo) + " , " +
                                Integer.toBinaryString(secondNumberDividedByPowOfTwo) + ") - вычли из второго - первый \n";
                    }
                }

                solution += " Ответ: НОД(а, b) = " + Integer.toBinaryString(PrimeFactorization.raiseToPow(2, kCommonDegree))  + " * " +Integer.toBinaryString(firstNumberDividedByPowOfTwo)
                        + " = " + Integer.toBinaryString(PrimeFactorization.raiseToPow(2, kCommonDegree) * firstNumberDividedByPowOfTwo) + "(2)";

                solution += "\nПроверка: \n" + "Первое число: " + firstBinaryNumber + "(2) = " + Integer.parseInt(firstBinaryDividedByPowOfTwo, 2) + "(10)"
                        + "\nВторое число: "+ secondBinaryNumber + "(2) = " + Integer.parseInt( secondBinaryDividedByPowOfTwo, 2) + "(10)"
                        + "\n HOD(" + Integer.parseInt(firstBinaryNumber, 2) + " , " + Integer.parseInt(secondBinaryNumber, 2)
                        + ") = " + PrimeFactorization.egcd(Integer.parseInt(firstBinaryNumber, 2) , Integer.parseInt( secondBinaryNumber, 2))
                        + "(10) = " + Integer.toBinaryString(PrimeFactorization.egcd(Integer.parseInt(firstBinaryNumber, 2) , Integer.parseInt( secondBinaryNumber, 2)))
                        + "(2)";

                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}
