package algorithmicsapplication.app.FirstTasks;

import java.util.List;

/**
 * Created by Nick on 05/01/2015.
 */
public class TableGenerator {

    public void tableGeneration(int biggerNumber, int smallerNumber, int iteration, List<AlgorithmTableRow> rows)
    {
        if(biggerNumber < smallerNumber)
        {
            int temp = biggerNumber;
            biggerNumber = smallerNumber;
            smallerNumber = temp;
        }

        int currentUi = 0;
        int currentUiplus1 = 0;
        int currentVi = 0;
        int currentViplus1 = 0;
        int currentRi = 0;
        int currentRiplus1 = 0;
        int currentQ = 0;
        boolean isEnded = false;

        if (iteration == 0)
        {
            currentQ = 0;
            currentUi = 1;
            currentVi = 0;
            currentRi = biggerNumber;
            currentUiplus1 = 0;
            currentViplus1 = 1;
            currentRiplus1 = smallerNumber;
            AlgorithmTableRow row = new AlgorithmTableRow(iteration, currentQ, currentUi, currentVi, currentRi, currentUiplus1, currentViplus1, currentRiplus1);
            rows.add(row);
        }
        if (iteration == 1)
        {
            currentQ =rows.get(0).rI/rows.get(0).rIplus1;
            currentUi = 0;
            currentVi = 1;
            currentRi = rows.get(0).rIplus1;
            currentUiplus1 = 1 - (currentQ * currentUi);
            currentViplus1 = 0 - (currentQ * 1);
            currentRiplus1 = smallerNumber;
            AlgorithmTableRow row = new AlgorithmTableRow(iteration, currentQ, currentUi, currentVi, currentRi, currentUiplus1, currentViplus1, currentRiplus1);
            rows.add(row);
            if (currentRiplus1 == 0)
                isEnded = true;
        }
        if (iteration >= 2)
        {
            currentQ = rows.get(iteration - 1).rI / rows.get(iteration - 1).rIplus1;
            currentUi = rows.get(iteration - 2).uI - rows.get(iteration - 1).q * rows.get(iteration - 1).uI;
            currentVi = rows.get(iteration - 2).vI - rows.get(iteration - 1).q * rows.get(iteration - 1).vI;
            currentRi = rows.get(iteration - 1).rIplus1;
            currentUiplus1 = rows.get(iteration - 2).uIplus1 - currentQ * rows.get(iteration - 1).uIplus1;
            currentViplus1 = rows.get(iteration - 2).vIplus1 - currentQ * rows.get(iteration - 1).vIplus1;
            currentRiplus1 = rows.get(iteration - 1).rI % currentRi;
            AlgorithmTableRow row = new AlgorithmTableRow(iteration, currentQ, currentUi, currentVi, currentRi, currentUiplus1, currentViplus1, currentRiplus1);
            rows.add(row);
            if (currentRiplus1 == 0)
                isEnded = true;
        }
        if (!isEnded)
        {
            iteration++;
            tableGeneration(smallerNumber, biggerNumber % smallerNumber, iteration, rows);
        }
    }

}
