package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 06/01/2015.
 */
public class Sem1_Task21_SystemComparisons extends ActionBarActivity {

    ArrayList<EditText> editTextFields = new ArrayList<EditText>();

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task21);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task21_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Решить систему сравнений, используя метод последовательного решения пары сравнений");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("Сгенереуйте необходимое количество полей и введите ЧЕРЕЗ ЗАПЯТЫЕ 3 числа (a, b, m), сравнения виде a * x = b(mod m)");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        Button addNewFieldButton = new Button(this);
        addNewFieldButton.setText("Добавить дополнительное поле");
        layout.addView(addNewFieldButton);
        addNewFieldButton.setOnClickListener(new AddNewFieldsListener(layout, this));

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new ExecutionListener(layout, resultTextView, this));
    }

    public class ExecutionListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        Context context;

        public ExecutionListener(LinearLayout linearLayout, TextView resultTextView, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            SystemComparisons systemComparisons = new SystemComparisons();
            String solution = "Рассматривается система сравнений: \n";
            try
            {
                for(int i = 0; i < editTextFields.size(); i++)
                {
                    String inputString = editTextFields.get(i).getText().toString();
                    String[] components = inputString.split(",");
                    systemComparisons.AddComparisons(new ComparisonByMod( Integer.parseInt(components[0]),
                            Integer.parseInt(components[1]), Integer.parseInt(components[2])));
                    solution += "(" + Integer.parseInt(components[0]) + ") * x = (" + Integer.parseInt(components[1])
                            + ")(mod " + Integer.parseInt(components[2]) + ")\n";
                }
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                solution +=  systemComparisons.FindAnswer();
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }

    public class AddNewFieldsListener implements View.OnClickListener {
        LinearLayout linearLayout;
        Context context;

        public AddNewFieldsListener(LinearLayout linearLayout, Context context) {
            this.linearLayout = linearLayout;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            EditText newEditTextView = new EditText(context);
            newEditTextView.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            editTextFields.add(newEditTextView);
            linearLayout.addView(newEditTextView);
        }
    }
}
