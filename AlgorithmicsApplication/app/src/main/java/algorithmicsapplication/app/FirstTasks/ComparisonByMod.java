package algorithmicsapplication.app.FirstTasks;

/**
 * Created by Nick on 06/01/2015.
 */
public class ComparisonByMod {
    public int aCoefficient;
    public int bCoefficient;
    public int modValue;

    public ComparisonByMod(int aCoefficient, int bCoefficient, int modValue) {
        this.aCoefficient = aCoefficient;
        this.bCoefficient = bCoefficient;
        this.modValue = modValue;
    }

    public String reduceToOneCoefficientA() {
        String solution = "Исходное сравнение: (" + aCoefficient + ") * x = (" + bCoefficient + ")(mod " +
                modValue + "); => домножив на обратный элемент относительно коэффициента 'a' при неизвестной 'x', имеем:\n";
        int reversedACoefficientByMod = PrimeFactorization.getReversedToNumberByMod(aCoefficient, modValue);
        solution += "x = (" + bCoefficient + ") * (" + reversedACoefficientByMod + ")(mod " + modValue + ")";
        int bCoefficientMultipliedByReversedA = bCoefficient * reversedACoefficientByMod;
        solution += "= (" + bCoefficientMultipliedByReversedA + ")(mod " + modValue + ")";
        int bCoefficientReductedByMod = bCoefficientMultipliedByReversedA % modValue;
        solution += "= (" + bCoefficientReductedByMod + ")(mod " + modValue + ");\n";
        bCoefficient = bCoefficientReductedByMod;
        aCoefficient = 1;
        return solution;
    }

    public ComparisonByMod enterToAnotherCopmarison(ComparisonByMod anrotherComparisonByMod) {
        // Если взять два ПРИВЕДЕННЫХ сравнения(коэффициент при х = 1),
        //то коэффициенты при подстановке одного сравнения в другое вычисляются следующим образом:
        // x= bNew(mod nMod) : bNew = b1 + m1 * (m1)^-1 * (b2 - b1)
        //modNew = m1 * m2;

//        int bNew = bCoefficient +
//                modValue * PrimeFactorization.getReversedToNumberByMod(modValue,
//
//                        anrotherComparisonByMod.modValue)
//                        * (anrotherComparisonByMod.bCoefficient - bCoefficient);
        int modNew = modValue * anrotherComparisonByMod.modValue;
        int bNew = bCoefficient + (anrotherComparisonByMod.bCoefficient * modValue);
        int adducedBnew = PrimeFactorization.getPositiveNumberByMod(bNew, modNew);
        ComparisonByMod newComparison = new ComparisonByMod(1, adducedBnew, modNew);
        return newComparison;
    }

    public String toDivideCoefficient()
    {
        String solution = "";
        if(PrimeFactorization.egcd(aCoefficient, bCoefficient) != 1)
        {
            int commonDividerAandB = PrimeFactorization.egcd(aCoefficient, bCoefficient);
            int commodDivierMandDividerAandB =  PrimeFactorization.egcd(commonDividerAandB, modValue);
            solution += this.toString() + " - можно сократить коэффициенты на " + commonDividerAandB
                    + ", при этом сократив модуль на '" + commodDivierMandDividerAandB + "';\n";
            aCoefficient = aCoefficient/commonDividerAandB;
            bCoefficient = bCoefficient/commonDividerAandB;
            modValue = modValue / commodDivierMandDividerAandB;
        }
        else
        {
            solution += "Нельзя сократить коэффициенты, поделив их;\n";
        }
        return solution;
    }
    public String ToString() {
        return "(" + aCoefficient + ") * x = (" + bCoefficient + ") (mod " + modValue + ")";
    }
}