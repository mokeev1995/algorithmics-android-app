package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 07/01/2015.
 */
public class Sem1_Task8_HideContinuedFraction extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task8);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task8_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Свернуть КОНЕЧНУЮ цепную (непрерывную) дробь");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите цепную дробь ЧЕРЕЗ ЗАПЯТЫЕ (без скобок)!");
        layout.addView(enterNumberA);
        EditText inputBoxFirstNumberA = new EditText(this);
        inputBoxFirstNumberA.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumberA);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumberA, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            String continuedFraction = firstNumberInputBox.getText().toString();
            ArrayList<Integer> qList = new ArrayList<Integer>();
            try
            {
                String[] qiInStringFormat = continuedFraction.split(",");
                for(int i = 0; i < qiInStringFormat.length; i++)
                {
                    qList.add(Integer.parseInt(qiInStringFormat[i]));
                }
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                String solution = "Рассматривается цепная дробь: [" + continuedFraction + "];\n";
                ArrayList<Integer> pList = new ArrayList<Integer>();
                ArrayList<Integer> QList = new ArrayList<Integer>();
                solution += "По определению: P[n] / Q[n] = [q1,...,qn];\n";
                solution += "А так же: P[0] = 1, Q[0] = 0, Q[1] = 1, P[1] = q1 = (" + qList.get(0) + ");\n";
                pList.add(1);
                pList.add(qList.get(0));
                QList.add(0);
                QList.add(1);
                int Pn = 0;
                int Qn = 0;
                for(int i = 2; i <= qList.size(); i++)
                {
                    int currentP = qList.get(i-1) * pList.get(i-1) + pList.get(i-2);
                    pList.add(currentP);
                    solution += "P[" + i + "] = q[" + i + "] * P[" + (i-1) + "] + P[" + (i-2) + "] = ("
                            + qList.get(i-1) +") * (" + pList.get(i-1) + ") + (" + pList.get(i-2) + ") = (" + currentP +");\n";
                    int currentQ = qList.get(i-1) * QList.get(i-1) + QList.get(i-2);
                    QList.add(currentQ);
                    solution += "Q[" + i + "] = q[" + i + "] * Q[" + (i-1) + "] + Q[" + (i-2) + "] = ("
                            + qList.get(i-1) +") * (" + QList.get(i-1) + ") + (" + QList.get(i-2) + ") = (" + currentQ +");\n";
                    if(i == qList.size())
                    {
                        Pn = currentP;
                        Qn = currentQ;
                    }
                }
                solution += "Исходная цепная дробь [" + continuedFraction + "] -является представление рациональной дроби: (" +
                        Pn + ") / (" + Qn + ");\n";
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}
