package algorithmicsapplication.app.FirstTasks;

import android.util.Pair;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Nick on 05/01/2015.
 */
public class GaussianNumbers {
    public int A;
    public int B;

    public GaussianNumbers(int a, int b) {
        A = a;
        B = b;
    }

    public String getNormInStringFormat() {
        return "g( (" + A + ") + (" + B + ")*i ) = " + A + "^2 + " + B + "^2 = " + getNorm();
    }

    public int getNorm() {
        return A * A + B * B;
    }

    public static GaussianNumbers multiply(GaussianNumbers g1, GaussianNumbers g2) {
        return new GaussianNumbers(g1.A * g2.A - g1.B * g2.B, g1.A * g2.B + g1.B * g2.A);
    }

    public static GaussianNumbers subtruct(GaussianNumbers g1, GaussianNumbers g2) {
        return new GaussianNumbers(g1.A - g2.A, g1.B - g2.B);
    }

    public static String DivideByEvenly(GaussianNumbers dividend, GaussianNumbers divider) {
        String solution = "Видно, что они делятся в Z[i]: \n";
        solution += "( " + dividend.toString() + " ) / ( " + divider.toString() + " ) = ";
        GaussianNumbers dividerWithNegativeImaginePart = new GaussianNumbers(divider.A, -(divider.B));
        solution += "(( " + dividend + " ) * ( " + dividerWithNegativeImaginePart + " )) / ";
        GaussianNumbers numerator = multiply(dividend, dividerWithNegativeImaginePart);
        int denominator = divider.A * divider.A + divider.B * divider.B;
        solution += "( " + denominator + " ) = ( " + numerator + " ) / " + denominator + " = ";
        solution += new GaussianNumbers(numerator.A / denominator, numerator.B / denominator).toString();
        return solution;
    }

    public static Pair<ArrayList<GaussianNumbers>, String> Divide(GaussianNumbers dividend, GaussianNumbers divider) {
        String solution = "( " + dividend.toString() + " ) / ( " + divider.toString() + " ) = ";
        GaussianNumbers dividerWithNegativeImaginePart = new GaussianNumbers(divider.A, -(divider.B));
        solution += "(( " + dividend + " ) * ( " + dividerWithNegativeImaginePart + " )) / ";
        GaussianNumbers numerator = multiply(dividend, dividerWithNegativeImaginePart);
        int denominator = divider.A * divider.A + divider.B * divider.B;
        solution += "( " + denominator + " ) = ( " + numerator + " ) / " + denominator + ";\n";
        solution += "Деление не выполняется в Z[i], т.к. знаменатель не делит одновременно вещественную и мнинмую часть; \n";
        double approximateRealPart = ((double) numerator.A) / ((double) denominator);
        double approximateImagingPart = ((double) numerator.B) / ((double) denominator);
        solution += "a' = ( " + numerator.A + ") / ( " + denominator + "), " +
                "b' = ( " + numerator.B + ") / ( " + denominator + " );\n";
        solution += "a' = " + approximateRealPart + ", b' = " + approximateImagingPart + ";\n";

        BigDecimal apporximateRealPartBigDecimal = new BigDecimal(approximateRealPart);
        BigDecimal apporximateImagingPartBigDecimal = new BigDecimal(approximateImagingPart);

        int integerApproximatedReadlPart = Integer.valueOf(apporximateRealPartBigDecimal.setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
        int integerApproximatedImagingPart = Integer.valueOf(apporximateImagingPartBigDecimal.setScale(0, BigDecimal.ROUND_HALF_UP).intValue());

        solution += "Соотвуствующие коээфициент в Z[i]: a = " + integerApproximatedReadlPart + ", b = " + integerApproximatedImagingPart
                + ";\n";
        GaussianNumbers aproximateQuotient = new GaussianNumbers(integerApproximatedReadlPart, integerApproximatedImagingPart);
        GaussianNumbers quotientMultipliedByDivider = multiply(aproximateQuotient, divider);
        GaussianNumbers restedGaussionNumber = subtruct(dividend, quotientMultipliedByDivider);
        ArrayList<GaussianNumbers> quotientAndRestNumber = new ArrayList<GaussianNumbers>();
        quotientAndRestNumber.add(aproximateQuotient);
        quotientAndRestNumber.add(restedGaussionNumber);
        Pair<ArrayList<GaussianNumbers>, String> pair = new Pair<ArrayList<GaussianNumbers>, String>(quotientAndRestNumber,
                solution);
        return pair;
    }

    public static boolean IsDividedEvenly(GaussianNumbers dividend, GaussianNumbers divider) {
        GaussianNumbers dividerWithNegativeImaginePart = new GaussianNumbers(divider.A, -(divider.B));
        GaussianNumbers numerator = multiply(dividend, dividerWithNegativeImaginePart);
        int denominator = divider.A * divider.A + divider.B * divider.B;
        if (numerator.A % denominator == 0 && numerator.B % denominator == 0) {
            return true;
        }
        return false;
    }

    public String toString() {
        return "(" + A + ") + (" + B + " ) * i";
    }
}

