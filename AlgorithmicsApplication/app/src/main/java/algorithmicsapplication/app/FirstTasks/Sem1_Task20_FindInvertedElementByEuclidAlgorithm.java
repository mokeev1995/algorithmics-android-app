package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 05/01/2015.
 */
public class Sem1_Task20_FindInvertedElementByEuclidAlgorithm extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task20);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task20_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Найти обратный элемент с помощью Алгоритма Евклида в Zm");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView aNumber = new TextView(this);
        aNumber.setText("Введите число a:");
        layout.addView(aNumber);
        EditText inputBoxANumber = new EditText(this);
        inputBoxANumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxANumber);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView mNumber = new TextView(this);
        mNumber.setText("Введите число m (Zm):");
        layout.addView(mNumber);
        EditText inputBoxMNumber = new EditText(this);
        inputBoxMNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxMNumber);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxANumber, inputBoxMNumber, this));
    }


    private LinearLayout generateTableCell(String text) {
        LinearLayout cell = new LinearLayout(this);
        TextView textView = new TextView(this);
        textView.setText(text);
        textView.setPadding(0, 0, 4, 3);
        cell.addView(textView);
        cell.setBackgroundColor(Color.GRAY);
        return cell;
    }


    public TableLayout GetTableLayout(List<AlgorithmTableRow> rows) {
        TableLayout table = new TableLayout(this);
        TableRow header = new TableRow(this);
        header.addView(generateTableCell("i"));
        header.addView(generateTableCell("q"));
        header.addView(generateTableCell("u[i]"));
        header.addView(generateTableCell("v[i]"));
        header.addView(generateTableCell("r[i]"));
        header.addView(generateTableCell("u[i+1]"));
        header.addView(generateTableCell("v[i+1]"));
        header.addView(generateTableCell("r[i+1]"));
        table.addView(header);
        for (int i = 0; i < rows.size(); i++) {
            TableRow tr = new TableRow(this);
            AlgorithmTableRow currentRow = rows.get(i);
            tr.addView(generateTableCell(Integer.toString(i)));
            tr.addView(generateTableCell(Integer.toString(currentRow.q)));
            tr.addView(generateTableCell(Integer.toString(currentRow.uI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.vI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.rI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.uIplus1)));
            tr.addView(generateTableCell(Integer.toString(currentRow.vIplus1)));
            tr.addView(generateTableCell(Integer.toString(currentRow.rIplus1)));
            table.addView(tr);
        }
        TableRow.LayoutParams param = new TableRow.LayoutParams();
        param.setMargins(0, 0, 0, 1);
        table.setLayoutParams(param);
        return table;
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText inputBoxANumber;
        EditText inputBoxMNumber;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText inputBoxANumber, EditText inputBoxMNumber, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.inputBoxANumber = inputBoxANumber;
            this.inputBoxMNumber = inputBoxMNumber;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

            int aNumber;
            int mNumber;
            try {
                aNumber = Integer.parseInt(inputBoxANumber.getText().toString());
                mNumber = Integer.parseInt(inputBoxMNumber.getText().toString());
            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }


            try {
                String solution = "Необходимо решить сравнение: " + aNumber + "*x = 1 (mod " + mNumber + ");\n";

                List<AlgorithmTableRow> rows = new ArrayList<AlgorithmTableRow>();

                TableGenerator tableGenerator = new TableGenerator();
                tableGenerator.tableGeneration(mNumber, aNumber, 0, rows); // ИЗМЕНЯЕТ List<AlgorithmTableRow> rows!!!
                int vN = rows.get(rows.size() - 1).vI;
                solution += "Обратным к '" + aNumber + "' - в Z" + mNumber + " - является: " + vN + "\n";
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
                linearLayout.addView(GetTableLayout(rows));
            } catch (Exception e) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}

