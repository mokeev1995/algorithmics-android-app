package algorithmicsapplication.app.FirstTasks;

/**
 * Created by Nick on 05/01/2015.
 */
public class AlgorithmTableRow {
    int i;
    int q;
    int uI;
    int vI;
    int rI;
    int uIplus1;
    int vIplus1;
    int rIplus1;

    public AlgorithmTableRow(int i, int q, int uI, int vI, int rI, int uIplus1, int vIplus1, int rIplus1) {
        this.i = i;
        this.q = q;
        this.uI = uI;
        this.vI = vI;
        this.rI = rI;
        this.uIplus1 = uIplus1;
        this.vIplus1 = vIplus1;
        this.rIplus1 = rIplus1;
    }
}


