package algorithmicsapplication.app.FirstTasks;

import android.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Nick on 04/01/2015.
 */
public class PrimeFactorization
{
    public static int getPositiveNumberByMod(int number, int mod) {
        if (number < 0) {
            number = number % mod;
            number = mod + number;
        }
        return number % mod;
    }

    public static boolean isHaveReversedToNumberByMod(int number, int mod)
    {
        for(int i = 1; i < mod; i++)
        {
            if(((number * i) % mod) == 1)
            {
                return true;
            }
        }
        return false;
    }


    public static int getReversedToNumberByMod(int number, int mod)
    {
        for(int i = 1; i < mod; i++)
        {
            if(((number * i) % mod) == 1)
            {
                return i;
            }
        }
        throw new ArithmeticException("getReversedToNumberByMod(int number, int mod) EXCEPTION");
    }

    public static int raiseToPowByMod(int base, int pow, int mod)
    {
        if(pow == 0 )
            return 1;

        int previousRest = base;
        for(int i = 2; i <= pow; i++)
        {
            previousRest = (previousRest * base) % mod;
        }
        return previousRest;
    }

    public static int raiseToPow(int base, int pow)
    {
        if(pow == 0 )
            return 1;

        int previousRest = base;
        for(int i = 2; i <= pow; i++)
        {
            previousRest = (previousRest * base);
        }
        return previousRest;
    }

    // Pair: base, pow
    public static ArrayList<Pair<Integer, Integer>> factorizeNumberToPrimeFactors(int n)
    {
        ArrayList<Integer> primeFactors = new ArrayList<Integer>();
        while(n != 1)
        {
            for(int i = 2; i <= n; i++)
            {
                if(isPrimeNumber(i))
                {
                    if(n % i == 0)
                    {
                        primeFactors.add(i);
                        n /= i;
                        break;
                    }
                }
            }
        }

        Comparator<Integer> comparator = new Comparator<Integer>() {
            public int compare(Integer firstNumber, Integer secondNumber) {
                return firstNumber.compareTo(secondNumber);
            }
        };

        Collections.sort(primeFactors, comparator);

        int previousNumber = primeFactors.get(0);
        int counterCurrentNumber = 1;
        ArrayList<Pair<Integer, Integer>> degreesList = new ArrayList<Pair<Integer, Integer>>();
        int amountPrimeFactors = primeFactors.size();
        if(amountPrimeFactors == 1)
        {
            // если факторизуется простое число
            degreesList.add(new Pair<Integer, Integer>(previousNumber, 1));
        }

        for(int i = 1; i < amountPrimeFactors; i++)
        {
            if(i ==  amountPrimeFactors - 1)
            {
                if(primeFactors.get(i) == previousNumber)
                {
                    counterCurrentNumber++;
                    degreesList.add(new Pair<Integer, Integer>(previousNumber, counterCurrentNumber));
                }
                else
                {
                    degreesList.add(new Pair<Integer, Integer>(previousNumber, counterCurrentNumber));
                    degreesList.add(new Pair<Integer, Integer>(primeFactors.get(i), 1));
                }
                break;
            }

            if(primeFactors.get(i) == previousNumber)
            {
                counterCurrentNumber++;
            }
            else
            {
                degreesList.add(new Pair<Integer, Integer>(previousNumber, counterCurrentNumber));
                previousNumber = primeFactors.get(i);
                counterCurrentNumber = 1;
            }
        }
        return degreesList;
    }

    public static int egcd(int a, int b) {
        if (a == 0)
            return b;

        while (b != 0) {
            if (a > b)
                a = a - b;
            else
                b = b - a;
        }

        return a;
    }

    public static boolean isCoprime(int firstNumber, int secondNumber)
    {
        int gcd = egcd(firstNumber, secondNumber);
        if(gcd != 1)
            return false;
        else
            return true;
    }

        public static boolean isPrimeNumber(int n)
        {
            for(int i = 2; i < n; i++)
            {
                if(n % i == 0)
                {
                    return false;
                }
            }
            return true;
        }

    public static int eulerFunction(int inputNumber)
    {
        List<Pair<Integer, Integer>> degreesList = PrimeFactorization.factorizeNumberToPrimeFactors(inputNumber);

        int eulerValue = 1;
        for(int i = 0; i < degreesList.size(); i++)
        {
            eulerValue *= PrimeFactorization.raiseToPow(degreesList.get(i).first, degreesList.get(i).second-1) * (degreesList.get(i).first - 1);
        }
        return eulerValue;
    }
}
