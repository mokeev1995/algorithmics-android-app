package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 10/01/2015.
 */
public class Sem1_Task10_HideIrrationalFraction extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task10);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task10_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Свернуть цепную БЕСКОНЕЧНУЮ ИРРАЦИОНАЛЬНУЮ дробь.");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите числа через запятую, которые НЕ содержатся в круглых скобках - через запятую без каких-либо скобок, иначе оставьте пустым.");
        layout.addView(enterNumberA);
        EditText inputBoxFirstNumberA = new EditText(this);
        inputBoxFirstNumberA.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumberA);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView textViewEnterSecondPart = new TextView(this);
        textViewEnterSecondPart .setText("Введите числа через запятую, которые СОДЕРЖАТСЯ в круглых скобках - через запятую без каких-либо скобок.");
        layout.addView(textViewEnterSecondPart );
        EditText inputBoxSecondPart = new EditText(this);
        inputBoxSecondPart.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxSecondPart);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumberA, inputBoxSecondPart, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText inputBoxSecondPart;
        Context context;
        String solution;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, EditText inputBoxSecondPart, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.inputBoxSecondPart = inputBoxSecondPart;
            this.context = context;
        }

        public IrrationalNumber getIrrationalNumberByPeriodicContinuedFraction(ArrayList<Integer> qList, String continuedFraction) {

            ArrayList<Integer> pList = new ArrayList<Integer>();
            ArrayList<Integer> QList = new ArrayList<Integer>();
            solution += "По определению: P[n] / Q[n] = [q1,...,qn];\n";
            solution += "А так же: P[0] = 1, Q[0] = 0, Q[1] = 1, P[1] = q1 = (" + qList.get(0) + ");\n";
            pList.add(1);
            pList.add(qList.get(0));
            QList.add(0);
            QList.add(1);
            int PnMinusOne = 0;
            int QnMinusOne = 0;
            int PnMinusTwo = 0;
            int QnMinusTwo = 0;
            for (int i = 2; i <= qList.size()+1; i++) {
                if (i == qList.size()+1) {
                    PnMinusOne = pList.get(i - 1);
                    QnMinusOne = QList.get(i - 1);
                    PnMinusTwo = pList.get(i - 2);
                    QnMinusTwo = QList.get(i - 2);
                    break;
                }

                int currentP = qList.get(i - 1) * pList.get(i - 1) + pList.get(i - 2);
                pList.add(currentP);
                solution += "P[" + i + "] = q[" + i + "] * P[" + (i - 1) + "] + P[" + (i - 2) + "] = ("
                        + qList.get(i - 1) + ") * (" + pList.get(i - 1) + ") + (" + pList.get(i - 2) + ") = (" + currentP + ");\n";
                int currentQ = qList.get(i - 1) * QList.get(i - 1) + QList.get(i - 2);
                QList.add(currentQ);
                solution += "Q[" + i + "] = q[" + i + "] * Q[" + (i - 1) + "] + Q[" + (i - 2) + "] = ("
                        + qList.get(i - 1) + ") * (" + QList.get(i - 1) + ") + (" + QList.get(i - 2) + ") = (" + currentQ + ");\n";

            }

            solution += "\nНеобходимо рассмотреть уравнение вида: alpha = (alpha * P[n-1] + P[n-2])/(alpha * Q[n-1] + Q[n-2]) :\n";
            QuadraticEquation quadraticEquation = new QuadraticEquation(QnMinusOne, (QnMinusTwo - PnMinusOne), -(PnMinusTwo));
            solution += quadraticEquation.solveEquation();
            IrrationalNumber irrationalNumberOfCurrentContinuedFraction = null;
            if (qList.get(0) > 0) {
                irrationalNumberOfCurrentContinuedFraction = quadraticEquation.getPositiveNumber();
                solution += "\nТ.к. первое неполное частное положительно, то берем положительный корень:\n"
                        + irrationalNumberOfCurrentContinuedFraction.printIrrationalNumber() + "\n";
            } else {
                irrationalNumberOfCurrentContinuedFraction = quadraticEquation.getNegativeNumber();

                solution += "\nТ.к. первое неполное частное отрицательн, то берем отрицательный корень:\n"
                        + irrationalNumberOfCurrentContinuedFraction.printIrrationalNumber() + "\n";
            }

            solution += "Исходная цепная дробь [" + continuedFraction + "] -является представление рациональной дроби: "
                    + irrationalNumberOfCurrentContinuedFraction.printIrrationalNumber() + ".\n";
            return irrationalNumberOfCurrentContinuedFraction;
        }

        public String calculateIrrationalNumberByContinuedFraction(IrrationalNumber irrationalNumber, ArrayList<Integer> quotientList, String totalContuniedFraction)
        {
            ArrayList<Integer> qList = quotientList;
            ArrayList<Integer> pList = new ArrayList<Integer>();
            ArrayList<Integer> QList = new ArrayList<Integer>();
            pList.add(1);
            pList.add(qList.get(0));
            QList.add(0);
            QList.add(1);
            for(int i = 2; i <= qList.size()+1; i++)
            {
                if(i == qList.size()+1)
                {
                    IrrationalNumber pnIrrational = IrrationalNumber.multiply(irrationalNumber, pList.get(i-1));
                    pnIrrational = IrrationalNumber.add(pnIrrational, pList.get(i-2));
                    solution += "P[" + i + "] = q[" + i + "] * P[" + (i-1) + "] + P[" + (i-2) + "] = ("
                            + irrationalNumber.printIrrationalNumber() +") * (" + pList.get(i-1) + ") + (" + pList.get(i-2) + ") = (" + pnIrrational.printIrrationalNumber()     +");\n";
                    IrrationalNumber qnIrrational = IrrationalNumber.multiply(irrationalNumber, QList.get(i-1));
                    qnIrrational = IrrationalNumber.add(qnIrrational, QList.get(i-2));
                    solution += "Q[" + i + "] = q[" + i + "] * Q[" + (i-1) + "] + Q[" + (i-2) + "] = ("
                            + irrationalNumber.printIrrationalNumber() +") * (" + QList.get(i-1) + ") + (" + QList.get(i-2) + ") = (" + qnIrrational.printIrrationalNumber() +");\n";

                    solution += "Исходная цепная дробь: [" + totalContuniedFraction + "] - представляет собой иррациональное число вида: ("
                            + pnIrrational.printIrrationalNumber() + ")/ (" + qnIrrational.printIrrationalNumber() + ")\n";
                    solution += "\nОТВЕТ: (" + pnIrrational.printIrrationalNumber() + ") / (" + qnIrrational.printIrrationalNumber() + ")\n";
                    solution += "\n\nНЕ ЗАБУДЬТЕ ПОЛУЧИВЩУЮСЯ ИРРАЦИОНАЛЬНУЮ ДРОБЬ САМОСТОЯТЕЛЬНО СОКРАТИТЬ, ЕСЛИ ПОЛУЧИТСЯ!";
                    break;
                }
                int currentP = qList.get(i-1) * pList.get(i-1) + pList.get(i-2);
                pList.add(currentP);
                solution += "P[" + i + "] = q[" + i + "] * P[" + (i-1) + "] + P[" + (i-2) + "] = ("
                        + qList.get(i-1) +") * (" + pList.get(i-1) + ") + (" + pList.get(i-2) + ") = (" + currentP +");\n";
                int currentQ = qList.get(i-1) * QList.get(i-1) + QList.get(i-2);
                QList.add(currentQ);
                solution += "Q[" + i + "] = q[" + i + "] * Q[" + (i-1) + "] + Q[" + (i-2) + "] = ("
                        + qList.get(i-1) +") * (" + QList.get(i-1) + ") + (" + QList.get(i-2) + ") = (" + currentQ +");\n";

            }
            return solution;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            solution = "";
            ArrayList<Integer> periodicQuotations = new ArrayList<Integer>();
            ArrayList<Integer> notPeriodicQuotations = new ArrayList<Integer>();

            try
            {
                String firstContinuedFraction = firstNumberInputBox.getText().toString();
                if(firstContinuedFraction.length() != 0)
                {
                    String[] notPeridicQuotationInStringFormat = firstContinuedFraction.split(",");
                    for(int i = 0; i < notPeridicQuotationInStringFormat.length; i++)
                    {
                        notPeriodicQuotations.add(Integer.parseInt(notPeridicQuotationInStringFormat[i]));
                    }
                }

                String secondContinuedFraction = inputBoxSecondPart.getText().toString();
                String[] periodicQuotationInStringFormat = secondContinuedFraction.split(",");
                for(int i = 0; i < periodicQuotationInStringFormat.length; i++)
                {
                    periodicQuotations.add(Integer.parseInt(periodicQuotationInStringFormat[i]));
                }
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                String periodicPart = "(";
                for(int i = 0; i < periodicQuotations.size(); i++)
                {
                    periodicPart += periodicQuotations.get(i);
                    if(i != periodicQuotations.size() -1 || i == 0)
                        periodicPart += ", ";
                }
                periodicPart += ")";

                String notPeriodicPart = "[";
                for(int i = 0; i < notPeriodicQuotations.size(); i++)
                {
                    notPeriodicPart += notPeriodicQuotations.get(i) + ", ";
                }
                notPeriodicPart += periodicPart + "]";
                solution += "\nРассматирвается следующая цепная дробь: " + notPeriodicPart + ".\n----------\n";

                IrrationalNumber irrationalNumberOfPeriodicContinuedFraction = getIrrationalNumberByPeriodicContinuedFraction(periodicQuotations, periodicPart);
                if(notPeriodicQuotations.size() != 0)
                {
                    solution += "\n\n------------\n" + calculateIrrationalNumberByContinuedFraction(irrationalNumberOfPeriodicContinuedFraction, notPeriodicQuotations, notPeriodicPart);
                }
                solution += "\nКорректность алгоритма можете проверить вбив в WolframAlpha - получившуюся иррациональность"
                        + " и посмотреть разложение в цепную дробь в пункте 'Continued Fraction' - причем ввиду специфики"
                        + " обозначений этого сервиса - обозначение из пособия [(a1, a2, a3)] - будет выглядеть:"
                        + "[a1, (a2, a3, a1)]";
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}