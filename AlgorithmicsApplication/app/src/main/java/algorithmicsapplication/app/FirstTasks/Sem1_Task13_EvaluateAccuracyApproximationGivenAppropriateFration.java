package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 08/01/2015.
 */
public class Sem1_Task13_EvaluateAccuracyApproximationGivenAppropriateFration extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task13);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task13_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Оценить точность приближения ИРРАЦИОНАЛЬНОСТИ - данной подходящей дробью P[k]/Q[k]:");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("В ДАННОЙ задаче - необходимо ввести только заданную цепную дробь.");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите цепную дробь ЧЕРЕЗ ЗАПЯТЫЕ (без скобок)!");
        layout.addView(enterNumberA);
        EditText inputBoxFirstNumberA = new EditText(this);
        inputBoxFirstNumberA.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumberA);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumberA, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            ArrayList<Integer> qList = new ArrayList<Integer>();
            String continuedFraction = firstNumberInputBox.getText().toString();
            try
            {
                String[] qiInStringFormat = continuedFraction.split(",");
                for (int i = 0; i < qiInStringFormat.length; i++) {
                    qList.add(Integer.parseInt(qiInStringFormat[i]));
                }
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                String solution = "Рассматривается цепная дробь: [" + continuedFraction + "];\n";
                ArrayList<Integer> QList = new ArrayList<Integer>();
                solution += "По определению: Q[0] = 0, Q[1] = 1;\n";
                QList.add(0);
                QList.add(1);
                int Qn = 0;
                for (int i = 2; i <= qList.size(); i++) {
                    int currentQ = qList.get(i - 1) * QList.get(i - 1) + QList.get(i - 2);
                    QList.add(currentQ);
                    solution += "Q[" + i + "] = q[" + i + "] * Q[" + (i - 1) + "] + Q[" + (i - 2) + "] = ("
                            + qList.get(i - 1) + ") * (" + QList.get(i - 1) + ") + (" + QList.get(i - 2) + ") = (" + currentQ + ");\n";
                    if (i == qList.size()) {
                        Qn = currentQ;
                    }
                }
                solution += "Q[" + (qList.size()) + "] = " + Qn;
                solution += "\nТогда точность приближения оценивается по формуле: |a - (Pk)/(Qk) | < ( 1 / ((Qk)^2) );\n";
                solution += "1/( (Q[" + (qList.size()+1) + "])^2 ) = 1/(" + Qn + " * " + Qn + ")).";
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}