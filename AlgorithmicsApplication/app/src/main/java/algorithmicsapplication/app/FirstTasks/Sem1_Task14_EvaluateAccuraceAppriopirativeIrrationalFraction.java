package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 09/01/2015.
 */
public class Sem1_Task14_EvaluateAccuraceAppriopirativeIrrationalFraction extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task14);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task14_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Оценить близость k-ой подходящей дроби к иррациональному числу.");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("\n\n----------\n\nВНИМАНИЕ: вводить коэффициенты для конкретно вашего числа в виде: (a + d * sqrt(b))/b " +
                "- т.к. этого требует универсальность алгоритма для входных данных!\n\n------------\n");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView textViewEnterFirstNumber = new TextView(this);
        textViewEnterFirstNumber.setText("Введите коэффициент a:");
        layout.addView(textViewEnterFirstNumber);
        EditText inputBoxFirstNumber = new EditText(this);
        inputBoxFirstNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumber);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView textViewEnterSecondNumber = new TextView(this);
        textViewEnterSecondNumber.setText("Введите коэффициент d:");
        layout.addView(textViewEnterSecondNumber);
        EditText inputBoxSecondNumber = new EditText(this);
        inputBoxSecondNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxSecondNumber);

        TextView textViewEnterThirdNumber = new TextView(this);
        textViewEnterThirdNumber.setText("Введите коэффициент b:");
        layout.addView(textViewEnterThirdNumber);
        EditText inputBoxThirdNumber = new EditText(this);
        inputBoxThirdNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxThirdNumber);

        TextView textViewEnterFourthNumber = new TextView(this);
        textViewEnterFourthNumber.setText("Введите коэффициент с:");
        layout.addView(textViewEnterFourthNumber);
        EditText inputBoFourthNumber = new EditText(this);
        inputBoFourthNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoFourthNumber);

        TextView textViewEnterNumberIteration = new TextView(this);
        textViewEnterNumberIteration.setText("Введите число k:");
        layout.addView(textViewEnterNumberIteration);
        EditText inputBoxNumberIteration = new EditText(this);
        inputBoxNumberIteration.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxNumberIteration);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumber, inputBoxSecondNumber, inputBoxThirdNumber, inputBoFourthNumber, inputBoxNumberIteration, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText secondNumberInputBox;
        EditText thirdNumberInputBox;
        EditText fourthNumberInputBox;
        EditText inputBoxNumberIteration;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, EditText secondNumberInputBox, EditText thirdNumberInputBox, EditText fourthNumberInputBox, EditText inputBoxNumberIteration, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.secondNumberInputBox = secondNumberInputBox;
            this.thirdNumberInputBox = thirdNumberInputBox;
            this.fourthNumberInputBox = fourthNumberInputBox;
            this.inputBoxNumberIteration = inputBoxNumberIteration;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            String solution = "";

            int coefficientA;
            int coefficientD;
            int coefficientB;
            int coefficientC;
            int numberIteration;
            try
            {
                coefficientA = Integer.parseInt(firstNumberInputBox.getText().toString());
                coefficientD = Integer.parseInt(secondNumberInputBox.getText().toString());
                coefficientB = Integer.parseInt(thirdNumberInputBox.getText().toString());
                coefficientC = Integer.parseInt(fourthNumberInputBox.getText().toString());
                numberIteration = Integer.parseInt(inputBoxNumberIteration.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                solution = "Рассматривается иррациональное число вида: ((" + coefficientA + ") + (" +
                        coefficientD + ") * Sqrt(" + coefficientB + ") )/ (" + coefficientC + ");\nРешение:\n";
                IrrationalNumber irrationalNumber = new IrrationalNumber(coefficientA, coefficientD, coefficientB, coefficientC, 1);

                irrationalNumber.compute();
                solution += irrationalNumber.solution;
                ArrayList<IrrationalNumber> irrationalNumbers = new ArrayList<IrrationalNumber>();
                irrationalNumbers.add(irrationalNumber);
                irrationalNumbers.add(irrationalNumber.reversedFractionalPartForAssignment);

                ArrayList<Integer> quotientes = new ArrayList<Integer>();
                int factualPeriodForCurrentIrrationalFraction = 0;
                String continuedFraction = "";
                while(true)
                {
                    solution += "----------------------\n";
                    int iteration = irrationalNumber.iteration + 1;
                    irrationalNumber = irrationalNumber.reversedFractionalPartForAssignment;
                    irrationalNumber.iteration = iteration;
                    irrationalNumber.compute();
                    solution += irrationalNumber.solution;
                    if(isContainsInList(irrationalNumbers, irrationalNumber.reversedFractionalPartForAssignment))
                    {
                        solution += "\n\n" + irrationalNumber.reversedFractionalPartForAssignment.printIrrationalNumber() +
                                " - уже был, поэтому алгоритм заканчивает свою работу!";
                        solution += "\n\nОТВЕТ: \n[";

                        for(int i = 0; i < irrationalNumbers.size(); i++)
                        {
                            if(irrationalNumber.reversedFractionalPartForAssignment.equals(irrationalNumbers.get(i)))
                            {
                                continuedFraction += "(" + irrationalNumbers.get(i).integerPart;
                                factualPeriodForCurrentIrrationalFraction = irrationalNumbers.size() - i;
                                quotientes.add(irrationalNumbers.get(i).integerPart);
                            }
                            else
                            {
                                continuedFraction += irrationalNumbers.get(i).integerPart;
                                quotientes.add(irrationalNumbers.get(i).integerPart);
                            }

                            if(i != irrationalNumbers.size() -1)
                                continuedFraction += ", ";
                        }
                        solution += continuedFraction + ")];\n\nПериод равен: '" + factualPeriodForCurrentIrrationalFraction + "'.\n";
                        break;
                    }
                    irrationalNumbers.add(irrationalNumber.reversedFractionalPartForAssignment);
                }

                solution += "\nРазвернем для заданного значения 'k' = " + numberIteration + "' - получившуюся цепную дробь: '" +
                        continuedFraction +"':\n[";

                ArrayList<Integer> quotientesForEntertingNumberIteration = new ArrayList<Integer>();
                int indexBegingOfPeriod = quotientes.size() - factualPeriodForCurrentIrrationalFraction;
                ArrayList<Integer> listOfPeriodicQuotientes = new ArrayList<Integer>();
                for(int j = 0; j < 10; j++)// ПАРАНОЙЯ! НА ВСЯКИЙ СЛУЧАЙ!
                {
                    for(int i = indexBegingOfPeriod; i < quotientes.size(); i++)
                    {
                        listOfPeriodicQuotientes.add(quotientes.get(i));
                    }
                }

                for(int i = 0; i < numberIteration; i++)
                {
                    if(i < indexBegingOfPeriod)
                    {
                        quotientesForEntertingNumberIteration.add(quotientes.get(i));
                        solution += quotientes.get(i);
                    }
                    else
                    {
                        quotientesForEntertingNumberIteration.add(listOfPeriodicQuotientes.get(i + indexBegingOfPeriod));
                        solution += listOfPeriodicQuotientes.get(i + indexBegingOfPeriod);
                    }

                    if(i != numberIteration-1)
                        solution += ", ";
                }
                solution += "] - по этой цепной дроби соотвественно необходимо найти P["+ numberIteration + "], Q["+ numberIteration +
                        "]" + ":\n";

                ArrayList<Integer> pList = new ArrayList<Integer>();
                ArrayList<Integer> QList = new ArrayList<Integer>();
                solution += "По определению: P[n] / Q[n] = [q1,...,qn];\n";
                solution += "А так же: P[0] = 1, Q[0] = 0, Q[1] = 1, P[1] = q1 = (" + quotientesForEntertingNumberIteration.get(0) + ");\n";
                pList.add(1);
                pList.add(quotientesForEntertingNumberIteration.get(0));
                QList.add(0);
                QList.add(1);
                int Pn = 0;
                int Qn = 0;
                for(int i = 2; i <= quotientesForEntertingNumberIteration.size(); i++)
                {
                    int currentP = quotientesForEntertingNumberIteration.get(i-1) * pList.get(i-1) + pList.get(i-2);
                    pList.add(currentP);
                    solution += "P[" + i + "] = q[" + i + "] * P[" + (i-1) + "] + P[" + (i-2) + "] = ("
                            + quotientesForEntertingNumberIteration.get(i-1) +") * (" + pList.get(i-1) + ") + (" + pList.get(i-2) + ") = (" + currentP +");\n";
                    int currentQ = quotientesForEntertingNumberIteration.get(i-1) * QList.get(i-1) + QList.get(i-2);
                    QList.add(currentQ);
                    solution += "Q[" + i + "] = q[" + i + "] * Q[" + (i-1) + "] + Q[" + (i-2) + "] = ("
                            + quotientesForEntertingNumberIteration.get(i-1) +") * (" + QList.get(i-1) + ") + (" + QList.get(i-2) + ") = (" + currentQ +");\n";
                    if(i == quotientesForEntertingNumberIteration.size())
                    {
                        Pn = currentP;
                        Qn = currentQ;
                    }
                }

                solution += "\nСогласно формуле: | alpha - (Pn)/(Qn) | < 1/((Qn)^2), => "
                        + "|" + irrationalNumber.printIrrationalNumber() + " - " + Pn + " / " + Qn + " | < 1/(" +
                        Qn + " * " + Qn + ") = " + (Qn*Qn) + ".\n";
                solution += "Ответ: Точность приближения: 1 / (" + Qn + " * " + Qn + ");";

                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }

        public boolean isContainsInList(ArrayList<IrrationalNumber> irrationalNumbers, IrrationalNumber number)
        {
            for(int i = 0; i < irrationalNumbers.size(); i++)
            {
                // i != 0
                // для случая sqrt(3)+1 - дает неправильный результат, т.к. для этого случая просматривается
                // дробная часть и исходное иррациональное число, что не является правильным!
                if(irrationalNumbers.get(i).equals(number) && i != 0)
                    return true;
            }
            return false;
        }
    }
}