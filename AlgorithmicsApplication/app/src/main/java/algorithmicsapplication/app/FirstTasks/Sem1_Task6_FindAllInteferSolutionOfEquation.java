package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 05/01/2015.
 */
public class Sem1_Task6_FindAllInteferSolutionOfEquation extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task6);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task6_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Найти все целочисленные решения уравнения: a * x + b * y = c");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите число a: ");
        layout.addView(enterNumberA);
        EditText inputBoxFirstNumberA = new EditText(this);
        inputBoxFirstNumberA.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumberA);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView enterNumberP = new TextView(this);
        enterNumberP.setText("Введите число b");
        layout.addView(enterNumberP);
        EditText inputBoxSecondNumberB = new EditText(this);
        inputBoxSecondNumberB.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxSecondNumberB);

        TextView thirdNumberViewBox = new TextView(this);
        thirdNumberViewBox.setText("Введите число c");
        layout.addView(thirdNumberViewBox);
        EditText inputThirdNumberBox = new EditText(this);
        inputThirdNumberBox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputThirdNumberBox);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumberA, inputBoxSecondNumberB, inputThirdNumberBox, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText secondNumberInputBox;
        EditText thirdNumberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, EditText secondNumberInputBox, EditText thirdNumberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.secondNumberInputBox = secondNumberInputBox;
            this.thirdNumberInputBox = thirdNumberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            int firstNumber;
            int secondNumber;
            int thirdNumber;
            try
            {
                firstNumber = Integer.parseInt(firstNumberInputBox.getText().toString());
                secondNumber = Integer.parseInt(secondNumberInputBox.getText().toString());
                thirdNumber = Integer.parseInt(thirdNumberInputBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                List<AlgorithmTableRow> rows = new ArrayList<AlgorithmTableRow>();

                TableGenerator tableGenerator = new TableGenerator();

                if(firstNumber > secondNumber)
                {
                    tableGenerator.tableGeneration(firstNumber, secondNumber, 0, rows); // ИЗМЕНЯЕТ List<AlgorithmTableRow> rows!!!
                }
                else
                {
                    tableGenerator.tableGeneration(secondNumber, firstNumber, 0, rows); // ИЗМЕНЯЕТ List<AlgorithmTableRow> rows!!!
                }
                linearLayout.addView(GetTableLayout(rows));

                AlgorithmTableRow lastRow = rows.get(rows.size()-1);
                int uN = lastRow.uI;
                int vN = lastRow.vI;
                int gcd = lastRow.rI;
                String solution = "Рассматривается уравенние: " + firstNumber + " * x + " + secondNumber + " * y = " + thirdNumber + ";\n";
                solution += "По расширенному алгоритму Евклида составим табличку: \n";
                solution += "HOД(" + firstNumber + " , " + secondNumber + ") = (" + firstNumber + ") * (" +
                        uN + ") + (" + secondNumber + ") * (" + vN + ") = " + gcd + ";\n";

                if(thirdNumber % gcd == 0)
                {
                    solution += "Уравенение: (" + firstNumber + ") * x + (" + secondNumber + ")* y = " + thirdNumber +
                            " - РАЗРЕШИМО в целых числах, т.к. НОД, равный: " + gcd + " делит " + thirdNumber  + ";\n";
                    int firstDividedByGcd = (firstNumber/gcd);
                    int secondDividedByGcd = (secondNumber/gcd);
                    int thirdDividedByGcd = (thirdNumber/gcd);
                    solution += "Разделим исходное уравнение на НОД(a, b): (" + firstDividedByGcd + ") * x"
                            + " + (" + secondDividedByGcd + ") * y = " + thirdDividedByGcd + ";\n";
                    if(PrimeFactorization.isCoprime(firstDividedByGcd, secondDividedByGcd))
                    {
                        if(firstNumber > secondNumber)
                        {
                            solution += "Т.к. числа:" + firstDividedByGcd + " и " + secondDividedByGcd + " - взаимопросты, то"
                                    + " они удовлетворяют равенству: (" + firstDividedByGcd + ") * u + (" + secondDividedByGcd
                                    + ") * v  = 1; \n Найдем решение этого равенства: \n";
                        }
                        else
                        {
                            solution += "Т.к. числа:" + firstDividedByGcd + " и " + secondDividedByGcd + " - взаимопросты, то"
                                    + " они удовлетворяют равенству: (" + firstDividedByGcd + ") * v + (" + secondDividedByGcd
                                    + ") * u  = 1; \n Найдем решение этого равенства: \n";
                        }
                        List<AlgorithmTableRow> additionalRows = new ArrayList<AlgorithmTableRow>();

                        TableGenerator currentTableGenerator = new TableGenerator();
                        if(firstNumber > secondNumber)
                        {
                            currentTableGenerator.tableGeneration(firstDividedByGcd, secondDividedByGcd, 0, additionalRows); // ИЗМЕНЯЕТ List<AlgorithmTableRow> rows!!!
                        }
                        else
                        {
                            currentTableGenerator.tableGeneration(secondDividedByGcd, firstDividedByGcd, 0, additionalRows); // ИЗМЕНЯЕТ List<AlgorithmTableRow> rows!!!
                        }

                        linearLayout.addView(GetTableLayout(additionalRows));

                        AlgorithmTableRow currentLastRow = additionalRows.get(additionalRows.size()-1);



                        int uN2 = currentLastRow.uI;
                        int vN2 = currentLastRow.vI;
                        int partialDisionUn = uN2 * thirdDividedByGcd;
                        int partialDisionVn = vN2 * thirdDividedByGcd;
                        if(firstNumber > secondNumber)
                        {
                            solution += "Решение равенства:  " + "(" + firstDividedByGcd + ") * u + (" + secondDividedByGcd
                                    + ") * v  = 1 - является (после домножения на " + thirdDividedByGcd + "): u0 = " + uN2 + " и v0 = " + vN2 +  ";\n";
                            solution+= "Решение ИСХОДНОГО равенства, ДЕЛЕННОГО НА НОД:  " + "(" + firstDividedByGcd + ") * " +
                                    "( " + thirdDividedByGcd + " * u) + "
                                    + " (" + secondDividedByGcd + ") * ( " + thirdDividedByGcd + " * v) = " + thirdDividedByGcd +
                                    " - является: x0 = " + partialDisionUn + " и y0 = " + partialDisionVn +  ";\n";
                            solution += "Т.к. " + partialDisionUn + ", " + partialDisionVn + " частное решение этого уравнения, то другие решения имеют вид:"
                                    + "x = " + partialDisionUn  + "- ((" + secondNumber + ") / (" + gcd + ") ) * t"
                                    + " = " + partialDisionUn  + "- (" + secondDividedByGcd + " ) * t.\n"
                                    + "y = " + partialDisionVn  + "+ ((" + firstNumber + ") / (" + gcd + ") ) * t"
                                    + " = " + partialDisionVn  + "+ (" + firstDividedByGcd + ") * t"
                                    + ", где t - принадлежит целым числам; \n";
                        }
                        else
                        {
                            solution += "Решение равенства:  " + "(" + firstDividedByGcd + ") * v + (" + secondDividedByGcd
                                    + ") * u  = 1 - является (после домножения на " + thirdDividedByGcd + "): v0 = " + vN2 + " и u0 = " + uN2 +  ";\n";
                            solution+= "Решение ИСХОДНОГО равенства, ДЕЛЕННОГО НА НОД:  " + "(" + firstDividedByGcd + ") * " +
                                    "( " + thirdDividedByGcd + " * v) + "
                                    + " (" + secondDividedByGcd + ") * ( " + thirdDividedByGcd + " * u) = " + thirdDividedByGcd +
                                    " - является: x0 = " +  partialDisionVn + " и y0 = " + partialDisionUn +  ";\n";
                            solution += "Т.к. " + partialDisionVn  + ", " + partialDisionUn + " частное решение этого уравнения, то другие решения имеют вид:"
                                    + "x = " + partialDisionVn  + "- ((" + secondNumber + ") / (" + gcd + ") ) * t"
                                    + " = " + partialDisionVn  + "- (" + secondDividedByGcd + " ) * t.\n"
                                    + "y = " + partialDisionUn  + "+ ((" + firstNumber+ ") / (" + gcd + ") ) * t"
                                    + " = " + partialDisionUn  + "+ (" + firstDividedByGcd + ") * t"
                                    + ", где t - принадлежит целым числам; \n";

                        }
                    }
                }
                else
                {
                    solution += "Урравнение " + firstNumber + "* x" + secondNumber + "* y = " + thirdNumber +
                            " - НЕ разрешимо в целых числах, т.к. НОД" + gcd + " НЕ делит " + thirdNumber  + ";\n";
                }
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }

    private LinearLayout generateTableCell(String text)
    {
        LinearLayout cell = new LinearLayout(this);
        TextView textView = new TextView(this);
        textView.setText(text);
        textView.setPadding(0, 0, 4, 3);
        cell.addView(textView);
        cell.setBackgroundColor(Color.GRAY);
        return cell;
    }

    private TableLayout GetTableLayout(List<AlgorithmTableRow> rows)
    {
        TableLayout table = new TableLayout(this);
        TableRow header = new TableRow(this);
        header.addView(generateTableCell("i"));
        header.addView(generateTableCell("q"));
        header.addView(generateTableCell("u[i]"));
        header.addView(generateTableCell("v[i]"));
        header.addView(generateTableCell("r[i]"));
        header.addView(generateTableCell("u[i+1]"));
        header.addView(generateTableCell("v[i+1]"));
        header.addView(generateTableCell("r[i+1]"));
        table.addView(header);
        for(int i = 0; i < rows.size(); i++)
        {
            TableRow tr = new TableRow(this);
            AlgorithmTableRow currentRow = rows.get(i);
            tr.addView(generateTableCell(Integer.toString(i)));
            tr.addView(generateTableCell(Integer.toString(currentRow.q)));
            tr.addView(generateTableCell(Integer.toString(currentRow.uI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.vI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.rI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.uIplus1)));
            tr.addView(generateTableCell(Integer.toString(currentRow.vIplus1)));
            tr.addView(generateTableCell(Integer.toString(currentRow.rIplus1)));
            table.addView(tr);
        }
        TableRow.LayoutParams param = new TableRow.LayoutParams();
        param.setMargins(0, 0, 0, 1);
        table.setLayoutParams(param);
        return table;
    }
}