package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 05/01/2015.
 */
public class Sem1_Task19_AllSolutionsOfComparison extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task19);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task19_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Используя расширенный алгоритм Евклида найти ВСЕ решения сравнения a * x = b(mod m)");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите число a, стоящее в левой части при х: ");
        layout.addView(enterNumberA);
        EditText inputBoxFirstNumberA = new EditText(this);
        inputBoxFirstNumberA.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumberA);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView enterNumberP = new TextView(this);
        enterNumberP.setText("Введите число b, стоящее слева");
        layout.addView(enterNumberP);
        EditText inputBoxSecondNumberB = new EditText(this);
        inputBoxSecondNumberB.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxSecondNumberB);

        TextView modNumberViewBox= new TextView(this);
        modNumberViewBox.setText("Введите число m - модуль");
        layout.addView(modNumberViewBox);
        EditText inputModNumber = new EditText(this);
        inputModNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputModNumber);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumberA, inputBoxSecondNumberB,inputModNumber, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText secondNumberInputBox;
        EditText modNumberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, EditText secondNumberInputBox, EditText modNumberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.secondNumberInputBox = secondNumberInputBox;
            this.modNumberInputBox = modNumberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            int firstNumber;
            int secondNumber;
            int modNumber;
            try
            {
                firstNumber = Integer.parseInt(firstNumberInputBox.getText().toString());
                secondNumber = Integer.parseInt(secondNumberInputBox.getText().toString());
                modNumber = Integer.parseInt(modNumberInputBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                List<AlgorithmTableRow> rows = new ArrayList<AlgorithmTableRow>();
                TableGenerator tableGenerator = new TableGenerator();
                tableGenerator.tableGeneration(modNumber, firstNumber, 0, rows); // ИЗМЕНЯЕТ List<AlgorithmTableRow> rows!!!
                linearLayout.addView(GetTableLayout(rows));

                AlgorithmTableRow lastRow = rows.get(rows.size()-1);
                int uN = lastRow.uI;
                int vN = lastRow.vI;
                int gcd = lastRow.rI;
                String solution = "Рассматривается сравнение вида: " + firstNumber + " * x = " + secondNumber + "(mod " + modNumber
                        + ");\n";
                if(secondNumber % gcd == 0)
                {
                    solution += "Коэффициенты Безу: u[n] = " + uN + ", v[n] = " + vN  + ";\n";
                    solution += "Выполняется равенство: " + gcd + " = (" + uN + ") * (" + modNumber + ") + (" + vN + ") * (" + firstNumber
                            + "); \n";
                    solution += "РЕШЕНИЕ ЕСТЬ, т.к. НОД, равный: " + gcd + " - делит " + secondNumber + ";\n";
                    solution += "HOD(" + modNumber + " , " + firstNumber + ") = " + gcd + ";\n";
                    int multiplicationCoeffecient = secondNumber / gcd;
                    solution += secondNumber + "/" + gcd + "=" + multiplicationCoeffecient+ ";\n";
                    solution += "Домножим на этот коэффициент равенство выше с коэффициентами Безу: \n";
                    solution += secondNumber + " = " + gcd + "*" + multiplicationCoeffecient + "=" + (uN *multiplicationCoeffecient) +
                            " * " + modNumber + " + " + (vN * multiplicationCoeffecient) + " * " + firstNumber  + ";\n";
                    int particularSolution = (vN * multiplicationCoeffecient);
                    solution += "Одно из решений: " + particularSolution+ ";\n";
                    solution += "Это решение : " + particularSolution + "единственно по модулю числа, равного: "
                            + modNumber + "/ НОД(" + modNumber + " , " + firstNumber + "), равного: "+ (modNumber/gcd) + ";\n";
                    solution += particularSolution + " =  " + PrimeFactorization.getPositiveNumberByMod(particularSolution, modNumber/gcd) +
                            "(mod " + modNumber/gcd  + ");\n";
                    solution += "Всего имеется: " + gcd + " решения по модулю " + modNumber + " (т.е. равно НОД'у;\n";
                    solution += "Остальные решения получаются последовательным прибавлением к частному решению '" + PrimeFactorization.getPositiveNumberByMod(particularSolution, modNumber/gcd)
                            + "' - чисел, полученного делением модуля на найденный НОД, т.е.:" + modNumber/gcd + ";\n";

                    for(int i = 0; i < gcd; i++)
                    {
                        solution += PrimeFactorization.getPositiveNumberByMod(particularSolution, modNumber/gcd) + " + (" +
                                modNumber/gcd + " * " + i + ");\n";
                    }

                    solution += "Всего получилось: " + gcd + " решений (это число равно найденному НОД'у \n";
                }
                else
                {
                    solution += "НЕТ РЕШЕНИЯ, т.к. " + gcd + " не делит " + secondNumber + ";\n";
                }
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }

    private LinearLayout generateTableCell(String text)
    {
        LinearLayout cell = new LinearLayout(this);
        TextView textView = new TextView(this);
        textView.setText(text);
        textView.setPadding(0, 0, 4, 3);
        cell.addView(textView);
        cell.setBackgroundColor(Color.GRAY);
        return cell;
    }

    private TableLayout GetTableLayout(List<AlgorithmTableRow> rows)
    {
        TableLayout table = new TableLayout(this);
        TableRow header = new TableRow(this);
        header.addView(generateTableCell("i"));
        header.addView(generateTableCell("q"));
        header.addView(generateTableCell("u[i]"));
        header.addView(generateTableCell("v[i]"));
        header.addView(generateTableCell("r[i]"));
        header.addView(generateTableCell("u[i+1]"));
        header.addView(generateTableCell("v[i+1]"));
        header.addView(generateTableCell("r[i+1]"));
        table.addView(header);
        for(int i = 0; i < rows.size(); i++)
        {
            TableRow tr = new TableRow(this);
            AlgorithmTableRow currentRow = rows.get(i);
            tr.addView(generateTableCell(Integer.toString(i)));
            tr.addView(generateTableCell(Integer.toString(currentRow.q)));
            tr.addView(generateTableCell(Integer.toString(currentRow.uI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.vI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.rI)));
            tr.addView(generateTableCell(Integer.toString(currentRow.uIplus1)));
            tr.addView(generateTableCell(Integer.toString(currentRow.vIplus1)));
            tr.addView(generateTableCell(Integer.toString(currentRow.rIplus1)));
            table.addView(tr);
        }
        TableRow.LayoutParams param = new TableRow.LayoutParams();
        param.setMargins(0, 0, 0, 1);
        table.setLayoutParams(param);
        return table;
    }
}
