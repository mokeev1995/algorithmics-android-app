package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Pair;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 04/01/2015.
 */
public class Sem1_Task23_EulerFunction extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task23);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task23_layout);
        EditText inputNumberBox = new EditText(this);
        inputNumberBox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        TextView resultTextView = new TextView(this);

        TextView taskCondition = new TextView(this);
        taskCondition.setText("Вычислить значение функции Эйлера от числа n:");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("F(n) = F(П p^ai) = П (pi^(ai-1) * (pi - 1) )");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        layout.addView(inputNumberBox);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener( layout, resultTextView, inputNumberBox, this));
    }

    public class CustomClickListener implements View.OnClickListener
    {
        LinearLayout layout;
        TextView resultTextView;
        EditText inputNumberBox;
        Context context;

        public CustomClickListener(LinearLayout layout, TextView resultTextView, EditText inputNumberBox, Context context) {
            this.layout = layout;
            this.resultTextView = resultTextView;
            this.inputNumberBox = inputNumberBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            int inputNumber;
            try
            {
                inputNumber = Integer.parseInt(inputNumberBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                if(PrimeFactorization.isPrimeNumber(inputNumber))
                {
                    resultTextView.setText(inputNumber + " - простое!");
                }
                else
                {
                    List<Pair<Integer, Integer>> degreesList = PrimeFactorization.factorizeNumberToPrimeFactors(inputNumber);
                    String degreesMultiplication = "n = ";
                    for(int i = 0; i < degreesList.size(); i++)
                    {
                        Pair<Integer, Integer> currentPair = degreesList.get(i);
                        degreesMultiplication += currentPair.first + "^" + currentPair.second;
                        if(i != degreesList.size() - 1)
                            degreesMultiplication +=  " * ";
                    }

                    int eulerValue = 1;
                    String eulerExpression = "";
                    for(int i = 0; i < degreesList.size(); i++)
                    {
                        eulerValue *= PrimeFactorization.raiseToPow(degreesList.get(i).first, degreesList.get(i).second-1) * (degreesList.get(i).first - 1);
                        eulerExpression += "( (" + degreesList.get(i).first + " ^ " + (degreesList.get(i).second-1) + ") * ( "
                                + degreesList.get(i).first + " - " + 1 + ") )";
                        if(i != degreesList.size()-1)
                            eulerExpression += " * ";
                        if(i == degreesList.size() - 1)
                            eulerExpression += " = " + eulerValue;
                    }
                    resultTextView.setText(degreesMultiplication + "\n|" + "Решение: " + eulerExpression);
                }
                layout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }


}
