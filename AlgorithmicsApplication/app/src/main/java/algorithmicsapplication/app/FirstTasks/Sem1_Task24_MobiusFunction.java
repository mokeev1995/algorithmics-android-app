package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Pair;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 04/01/2015.
 */
public class Sem1_Task24_MobiusFunction extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task24);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task24_layout);
        EditText inputNumberBox = new EditText(this);
        inputNumberBox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        TextView resultTextView = new TextView(this);

        TextView taskCondition = new TextView(this);
        taskCondition.setText("Вычислить значение функции Мебиуса от числа n:");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("1 - если n == 1; (-1)^r, где r - число простых сомножителей" +
        "причем все они входят в первой степени; 0 - в противном случае");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        layout.addView(inputNumberBox);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputNumberBox, this));
    }

    public class CustomClickListener implements View.OnClickListener
    {
        LinearLayout layout;
        TextView resultTextView;
        EditText inputNumberBox;
        Context context;

        public CustomClickListener(LinearLayout layout, TextView resultTextView, EditText inputNumberBox, Context context) {
            this.layout = layout;
            this.resultTextView = resultTextView;
            this.inputNumberBox = inputNumberBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            int inputNumber;
            try
            {
                inputNumber = Integer.parseInt(inputNumberBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                if(inputNumber == 1){
                    resultTextView.setText("F(n)= 1, т.к. n = 1");
                }
                else
                {
                    String solution = inputNumber + " = ";
                    List<Pair<Integer, Integer>> degreesList = PrimeFactorization.factorizeNumberToPrimeFactors(inputNumber);
                    for(int i = 0; i < degreesList.size(); i++)
                    {
                        solution += "(" + degreesList.get(i).first + "^" + degreesList.get(i).second + ")";
                        if(i != degreesList.size()-1)
                            solution += " * ";
                    }
                    for(int i = 0; i < degreesList.size(); i++)
                    {
                        if(degreesList.get(i).second != 1)
                        {
                            resultTextView.setText(solution + "F(n)= 0, т.к. содержится кратный простой сомножитель.");
                            layout.addView(resultTextView);
                            return;
                        }
                    }
                    solution += "\nF(" + inputNumber + ") = (-1)^" + degreesList.size();

                    if(degreesList.size() % 2 == 0)
                        solution += " = 1";
                    else
                        solution += " = (-1).";
                    resultTextView.setText(solution);
                }
                layout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}
