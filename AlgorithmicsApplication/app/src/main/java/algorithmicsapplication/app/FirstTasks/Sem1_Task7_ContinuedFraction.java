package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 07/01/2015.
 */
public class Sem1_Task7_ContinuedFraction extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task7);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task7_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Разложить РАЦИОНАЛЬНОЕ число в цепную (непрерывную) дробь");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите числитель");
        layout.addView(enterNumberA);
        EditText inputBoxFirstNumberA = new EditText(this);
        inputBoxFirstNumberA.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumberA);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView enterNumberP = new TextView(this);
        enterNumberP.setText("Введите знаменатель");
        layout.addView(enterNumberP);
        EditText inputBoxSecondNumberB = new EditText(this);
        inputBoxSecondNumberB.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxSecondNumberB);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumberA, inputBoxSecondNumberB, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText secondNumberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, EditText secondNumberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.secondNumberInputBox = secondNumberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            int numerator;
            int denominator;
            try
            {
                numerator  = Integer.parseInt(firstNumberInputBox.getText().toString());
                denominator = Integer.parseInt(secondNumberInputBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                String solution = "Рассматривается дробь: (" + numerator + ") / (" + denominator + ");\n";

                int currentQuotient = 0;
                int rest = 0;
                if(numerator < 0 || denominator < 0)
                {
                    currentQuotient = (numerator / denominator) -1;
                    rest = numerator - (currentQuotient * denominator);
                    solution += "Т.е. у нас дробь отрицательно, то необходимо добиться, чтобы q1 < 0 & q[i] > 0, где i=2..n \n";
                }
                else
                {
                    currentQuotient = numerator / denominator;
                    rest = numerator % denominator;
                }
                ArrayList<Integer> quotientList = new ArrayList<Integer>();
                quotientList.add(currentQuotient);
                while(true)
                {
                    solution += "(" + numerator + ") = (" + currentQuotient + ") * (" + denominator + ") + (" + rest + ");\n";
                    numerator = denominator;
                    denominator = rest;
                    currentQuotient = numerator / denominator;
                    rest = numerator % denominator;
                    quotientList.add(currentQuotient);
                    if(rest == 0)
                    {
                        solution += "(" + numerator + ") = (" + currentQuotient + ") * (" + denominator + ");\n";
                        break;
                    }
                }

                solution += "Исходная дробь в виде цепной дроби: [";
                for(int i = 0; i < quotientList.size(); i++)
                {
                    solution += quotientList.get(i);
                    if(i != quotientList.size()-1)
                        solution += ",";
                }
                solution += "]";
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}