package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 09/01/2015.
 */
public class Sem1_Task9_IrrationalToContinuedFraction extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task9);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task9_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Разложить ИРРАЦИОНАЛЬНОСТЬ в непрерывную дробь. Найти период");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("\n\n----------\n\nВНИМАНИЕ: вводить коэффициенты для конкретно вашего числа в виде: (a + d * sqrt(b))/b " +
                "- т.к. этого требует универсальность алгоритма для входных данных!\n\n------------\n");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView textViewEnterFirstNumber = new TextView(this);
        textViewEnterFirstNumber.setText("Введите коэффициент a:");
        layout.addView(textViewEnterFirstNumber);
        EditText inputBoxFirstNumber = new EditText(this);
        inputBoxFirstNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumber);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView textViewEnterSecondNumber = new TextView(this);
        textViewEnterSecondNumber.setText("Введите коэффициент d:");
        layout.addView(textViewEnterSecondNumber);
        EditText inputBoxSecondNumber = new EditText(this);
        inputBoxSecondNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxSecondNumber);

        TextView textViewEnterThirdNumber = new TextView(this);
        textViewEnterThirdNumber.setText("Введите коэффициент b:");
        layout.addView(textViewEnterThirdNumber);
        EditText inputBoxThirdNumber = new EditText(this);
        inputBoxThirdNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxThirdNumber);

        TextView textViewEnterFourthNumber = new TextView(this);
        textViewEnterFourthNumber.setText("Введите коэффициент d:");
        layout.addView(textViewEnterFourthNumber);
        EditText inputBoFourthNumber = new EditText(this);
        inputBoFourthNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoFourthNumber);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumber, inputBoxSecondNumber, inputBoxThirdNumber, inputBoFourthNumber, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText secondNumberInputBox;
        EditText thirdNumberInputBox;
        EditText fourthNumberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, EditText secondNumberInputBox, EditText thirdNumberInputBox, EditText fourthNumberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.secondNumberInputBox = secondNumberInputBox;
            this.thirdNumberInputBox = thirdNumberInputBox;
            this.fourthNumberInputBox = fourthNumberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            int coefficientA;
            int coefficientD;
            int coefficientB;
            int coefficientC;
            try
            {
                coefficientA = Integer.parseInt(firstNumberInputBox.getText().toString());
                coefficientD = Integer.parseInt(secondNumberInputBox.getText().toString());
                coefficientB = Integer.parseInt(thirdNumberInputBox.getText().toString());
                coefficientC = Integer.parseInt(fourthNumberInputBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                String solution = "Рассматривается иррациональное число вида: (" + coefficientA + ") + (" +
                        coefficientD + ") * Sqrt(" + coefficientB + ") )/ (" + coefficientC + ");\nРешение:\n";

                int iter = 1;
                IrrationalNumber number = new IrrationalNumber(coefficientA, coefficientD, coefficientB, coefficientC,  iter);
                number.compute();
                solution += number.solution;
                ArrayList<IrrationalNumber> irrationalNumbers = new ArrayList<IrrationalNumber>();
                irrationalNumbers.add(number);
                irrationalNumbers.add(number.reversedFractionalPartForAssignment);

                while(true)
                {
                    solution += "----------------------\n";
                    int iteration = number.iteration + 1;
                    number = number.reversedFractionalPartForAssignment;
                    number.iteration = iteration;
                    number.compute();
                    solution += number.solution;
                    if(isContainsInList(irrationalNumbers, number.reversedFractionalPartForAssignment))
                    {
                        solution += "\n\n" + number.reversedFractionalPartForAssignment.printIrrationalNumber() +
                                " - уже был, поэтому алгоритм заканчивает свою работу!";
                        solution += "\n\nОТВЕТ: \n[";
                        int period = 0;
                        for(int i = 0; i < irrationalNumbers.size(); i++)
                        {
                            if(number.reversedFractionalPartForAssignment.equals(irrationalNumbers.get(i)))
                            {
                                solution += "(" + irrationalNumbers.get(i).integerPart;
                                period = irrationalNumbers.size() - i;
                            }
                            else
                            {
                                solution += irrationalNumbers.get(i).integerPart;
                            }

                            if(i != irrationalNumbers.size() -1)
                                solution += ", ";
                        }
                        solution += ")];\n\nПериод равен: '" + period + "'.\n";
                        break;
                    }
                    irrationalNumbers.add(number.reversedFractionalPartForAssignment);
                }
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }

        public boolean isContainsInList(ArrayList<IrrationalNumber> irrationalNumbers, IrrationalNumber number)
        {
            for(int i = 0; i < irrationalNumbers.size(); i++)
            {
                // i != 0
                // для случая sqrt(3)+1 - дает неправильный результат, т.к. для этого случая просматривается
                // дробная часть и исходное иррациональное число, что не является правильным!
                if(irrationalNumbers.get(i).equals(number) && i != 0)
                    return true;
            }
            return false;
        }
    }
}