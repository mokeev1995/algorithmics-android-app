package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Pair;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 08/01/2015.
 */
public class Sem1_Task32_DescribeStructureMultiplicativeGroup extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task32);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task32_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Описать структуру мультипликативной группы кольца Zn. Найти порядок группы. Найти элемент макс. порядка");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("\n-----------------\nДопускает ДЛИТЕЛЬНАЯ ЗАДЕРЖКА во время вычислений!!!\n" +
                "-----------------\n");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите число n: ");
        layout.addView(enterNumberA);
        EditText inputBaseNumberABox = new EditText(this);
        inputBaseNumberABox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberABox);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBaseNumberABox, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText numberInputBox;
        String typeNumber = "";
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText numberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.numberInputBox = numberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

            int inputNumber;
            try
            {
                inputNumber = Integer.parseInt(numberInputBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                String solution = "";
                if (isNumberOfCyclicGroup(inputNumber)) {
                    solution += "Группа является циклической.";
                } else {
                    solution += "Порядок группы Z" + inputNumber + " - равен значению функции Эйлера от числа '" +
                            inputNumber +"' и равен: '" + PrimeFactorization.eulerFunction(inputNumber) + "';\n-------------\n";

                    List<Pair<Integer, Integer>> degreesList = PrimeFactorization.factorizeNumberToPrimeFactors(inputNumber);
                    solution += "Введенное число имеет следующее разложение на степени простых чисел:\n";
                    for (int i = 0; i < degreesList.size(); i++) {
                        solution += "(" + degreesList.get(i).first + ")^(" + degreesList.get(i).second + ")";
                        if(i != degreesList.size() -1)
                            solution += " * ";
                    }
                    solution += "\nТогда исходная нециклическая группа изоморфно декартову произведению" +
                            " следующих групп (в данном случае звездочка - это обозначение мультипликативной группы):\n";
                    solution += "Z*" + inputNumber + " ~=";
                    for (int i = 0; i < degreesList.size(); i++) {
                        solution += "Z*(" + degreesList.get(i).first + ")^(" + degreesList.get(i).second + ")";
                        if(i != degreesList.size() -1)
                            solution += " x ";
                    }
                    solution += "\n";
                    ArrayList<Integer> GeneratrixList = new ArrayList<Integer>();

                    for (int i = 0; i < degreesList.size(); i++) {
                        solution += "-------------\n";
                        int currentNumberOfSubgroup = PrimeFactorization.raiseToPow(degreesList.get(i).first, degreesList.get(i).second);
                        int orderCurrentSubgroup = PrimeFactorization.eulerFunction(currentNumberOfSubgroup);
                        solution += "Порядок группы Z" + degreesList.get(i).first + "^" + degreesList.get(i).second + " равен функции Эйлера от этого значения? т.е равен: '"
                                + orderCurrentSubgroup + "';\n-------------\n";
                        solution += "Поэтому необходимо искать элемент порядка: " + orderCurrentSubgroup + "в группе Z"
                                + degreesList.get(i).first + "^" + degreesList.get(i).second +"; \n";
                        for (int m = 2; m < currentNumberOfSubgroup; m++) {
                            int previosRest = m;
                            boolean isFindGeneratrix = false;
                            for (int j = 2; j <= orderCurrentSubgroup; j++) {
                                previosRest = PrimeFactorization.raiseToPowByMod(m, j, currentNumberOfSubgroup);
                                solution += m + "^" + j + "==" + previosRest + "(mod " + currentNumberOfSubgroup + "); \n";
                                if (previosRest == 1 && j == orderCurrentSubgroup) {
                                    solution += "Найдена образующая группы: '" + m + "'; \n";
                                    GeneratrixList.add(m);
                                    isFindGeneratrix = true;
                                    break;
                                }
                                if (previosRest == 1 && j != orderCurrentSubgroup) {
                                    solution += m + "не является образующей! \n";
                                    break;
                                }
                            }
                            if (isFindGeneratrix)
                                break;
                        }
                    }

                    solution += "\n\nНеобходимо построить биективное отоброжение:  f:";
                    String vectorOfGenerectix = "(";
                    for (int i = 0; i < degreesList.size(); i++) {
                        solution += "Z" + degreesList.get(i).first + "^" + degreesList.get(i).second;
                        if(i != degreesList.size()-1)
                            solution += " x ";
                        vectorOfGenerectix += GeneratrixList.get(i);
                        if(i != degreesList.size()-1)
                            vectorOfGenerectix += ", ";
                    }

                    solution += " -> Z" + inputNumber + "\n";
                    solution += "Из найденных образующих групп (составляющих декартово произведение исходной группы) - составляем набор" + vectorOfGenerectix
                            + " и строитеся отображение его в Z" + inputNumber + ";\n";
                    solution += "Для этого необходимо решить систему сравнений виде: x[i] = a[i](mod m[i])," +
                            ", где a[i] - это образующая i-ой группы разложения исходной группы в декартово произведение"
                            + ", a m[i] - это модуль рассматриваемой i-ой мультипликативной группы.\n";
                    solution += "Получаем следующую систему сравнений: \n";
                    SystemComparisons systemComparisons = new SystemComparisons();
                    for (int i = 0; i < degreesList.size(); i++) {
                        systemComparisons.AddComparisons(new ComparisonByMod(1,
                                GeneratrixList.get(i), PrimeFactorization.raiseToPow(degreesList.get(i).first, degreesList.get(i).second)));
                        solution += systemComparisons._Comparisons.get(i).ToString() + ";\n";
                    }

                    solution += "\nРешим эту систему сравнений:\n";
                    solution += systemComparisons.FindAnswer();
                    solution += "\nМы получили решение данной системы сравнений.\n";
                    solution += "\n-----------\nТогда 'ЭЛЕМЕНТОМ МАКСИМАЛЬНОГО ПОРЯДКА' - и будет является решением этой системы сравнений: '"
                            + systemComparisons.ResultingComparison.bCoefficient + "';\n";
                    int elementMaxOrderInSourceGroud = systemComparisons.ResultingComparison.bCoefficient;
                    solution += "\n-----------\nНайдем порядок этого элемента: \n";
                    int orderSourceGroup = PrimeFactorization.eulerFunction(inputNumber);
                    int currentRest = elementMaxOrderInSourceGroud;
                    int maxOrderInGroup = 0;
                    for (int i = 2; i < orderSourceGroup; i++) {
                        currentRest = PrimeFactorization.raiseToPowByMod(elementMaxOrderInSourceGroud,
                                i, inputNumber);
                        solution += elementMaxOrderInSourceGroud + "^" + i + "(mod " + inputNumber + ") = " + currentRest +";\n";
                        if(currentRest == 1)
                        {
                            maxOrderInGroup = i;
                            solution += "ФИНАЛЬНЫЙ ОТВЕТ:Порядок элемента (равный МАКСИМАЛЬНОМУ В ИСХОДНОЙ ГРУППЕ): '" + elementMaxOrderInSourceGroud + "' - равен '" + i + "';\n";
                            solution += "\nТ.е. ord(" + elementMaxOrderInSourceGroud + ") = " + maxOrderInGroup;
                            break;
                        }
                    }

                    String warning = "\n----------\nПроверка корректности алгоритма (полным перебором) - если найдется в данном списке" +
                            " степень, равная единице - показатель которой больше 'МАКСИМАЛЬНОМУ В ИСХОДНОЙ ГРУППЕ' - то значит программа работает не верно:\n";

                    for(int i = 2; i < inputNumber;i++)
                    {
                        int testRest = i;
                        for(int j = 2; j < orderSourceGroup; j++)
                        {
                            testRest = PrimeFactorization.raiseToPowByMod(i, j, inputNumber);
                            warning += i + "^" + j + "(mod " + inputNumber + ") = " + testRest
                                    + ";\n";
                            if(testRest == 1)
                            {
                                warning += "Порядок элемента '" + i + "' - равен: '" + j + "'\n;";
                                if(j > maxOrderInGroup)
                                {
                                    solution += "\n--------\nВ РЕШЕНИИ СУДЯ ПО ВСЕМУ ОШИБКА!!!\n" +
                                            "--------\n" + warning;
                                }

                                break;
                            }
                        }
                    }
                }

                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }

        private boolean isNumberOfCyclicGroup(int number) {
            List<Pair<Integer, Integer>> degrees = PrimeFactorization.factorizeNumberToPrimeFactors(number);
            if (number == 2) {
                typeNumber = " == 2;";

                return true;
            }
            if (number == 4) {
                typeNumber = " == 4";
                return true;
            }
            if (degrees.size() == 1) {
                //p^a
                if (degrees.get(0).second >= 1) {
                    typeNumber = " == p^a";
                    return true;
                } else
                    return false;
            }
            if (degrees.size() == 2) {
                //2p^a
                if (degrees.get(0).first == 2 && degrees.get(0).second == 1) {
                    if (degrees.get(1).second != 2 && degrees.get(1).second >= 1) {
                        typeNumber = " == 2p^a";
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
