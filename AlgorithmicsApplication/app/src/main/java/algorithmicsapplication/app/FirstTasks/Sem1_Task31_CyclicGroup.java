package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Pair;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 04/01/2015.
 */
public class Sem1_Task31_CyclicGroup extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task31);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task31_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Проверить группу на цикличность. Найти порядок группы. Найти образующую.");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите число n: ");
        layout.addView(enterNumberA);
        EditText inputBaseNumberABox = new EditText(this);
        inputBaseNumberABox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberABox);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBaseNumberABox, this));
    }

    public class CustomClickListener implements View.OnClickListener
    {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText numberInputBox;
        String typeNumber = "";
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText numberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.numberInputBox = numberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);
            int inputNumber;
            try
            {
                inputNumber = Integer.parseInt(numberInputBox.getText().toString());

            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                String solution = "Группа является циклической, если число n - имеет вид: 2, 4, p^a либо 2 * p^a; где p - простое, a >= 1.\n";
                if(isProperNumber(inputNumber))
                {
                    solution += "Введенное число соотвествует типу: " + typeNumber + ", поэтому группа является циклической; \n";
                    int groupOrder = PrimeFactorization.eulerFunction(inputNumber);
                    solution += "Порядок группы Z" + inputNumber + " равен функции Эйлера от этого значения:"
                            + groupOrder + "; \n";
                    solution += "Поэтому необходимо искать элемент порядка: " + groupOrder + "; \n";
                    solution += "-----------------------" + "\n"+ "\n";
                    for(int i = 2; i < inputNumber; i++)
                    {
                        int previosRest = i;
                        boolean isFindGeneratrix = false;
                        for(int j = 2; j <= groupOrder; j++)
                        {
                            previosRest = (previosRest * i) % inputNumber;
                            solution += i + "^" + j + "==" + previosRest + "(mod " + inputNumber + "); \n";
                            if (previosRest == 1 && j == groupOrder)
                            {
                                solution += "Найдена образующая группы: '" + i + "'; \n";
                                isFindGeneratrix = true;
                                break;
                            }
                            if (previosRest == 1 && j != groupOrder)
                            {
                                solution += i + "не является образующей! \n";
                                break;
                            }
                        }
                        if(isFindGeneratrix)
                            break;
                    }
                }
                else
                {
                    solution = "Число n не удволетворяет условию, что равно одному числу из 2, 4, p^a, 2p^a";
                }

                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }

        private boolean isProperNumber(int number)
        {
            List<Pair<Integer, Integer>> degrees = PrimeFactorization.factorizeNumberToPrimeFactors(number);
            if(number == 2)
            {
                typeNumber = "2;";

                return true;
            }
            if(number == 4) {
                typeNumber = "4";
                return true;
            }
            if(degrees.size() == 1)
            {
                //p^a
                if(degrees.get(0).second >= 1)
                {
                    typeNumber = "p^a";
                    return true;
                }
                else
                    return false;
            }
            if(degrees.size() == 2)
            {
                //2p^a
                if(degrees.get(0).first == 2 && degrees.get(0).second == 1)
                {
                    if(degrees.get(1).second != 2 && degrees.get(1).second >= 1)
                    {
                        typeNumber = "2p^a";
                        return true;
                    }
                }
            }
                return false;
        }
    }
}
