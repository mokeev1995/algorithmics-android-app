package algorithmicsapplication.app.FirstTasks;

/**
 * Created by Nick on 10/01/2015.
 */
/**
 * Created by Nick on 10/01/2015.
 */
public class QuadraticEquation {
    int a;
    int b;
    int c;
    int discriminant;
    IrrationalNumber firstDecision;
    IrrationalNumber secondDecision;

    public QuadraticEquation(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public String solveEquation()
    {
        String solution = "";
        discriminant = b*b - 4*(a * c);
        firstDecision = new IrrationalNumber();
        firstDecision.a = -b;
        firstDecision.d = 1;
        firstDecision.b = discriminant;
        firstDecision.c = 2*a;
        solution += "\n Первый корень имеет вид: ((-b + sqrt(disciminant))/2a) = ( ("
                + (-b) + ")+sqrt(" + discriminant + ") ) / (2*" + a + ") = "
                + firstDecision.printIrrationalNumber() + " = ";

        firstDecision.simplify();
        solution += firstDecision.printIrrationalNumber();

        secondDecision = new IrrationalNumber();
        secondDecision.a = -b;
        secondDecision.d = -1;
        secondDecision.b = discriminant;
        secondDecision.c = 2*a;
        solution += "\n Второй корень имеет вид: ((-b + sqrt(disciminant))/2a) = ( ("
                + (-b) + ")+sqrt(" + discriminant + ") ) / (2*" + a + ") = " + secondDecision.printIrrationalNumber();

        secondDecision.simplify();
        solution += " = " + secondDecision.printIrrationalNumber();
        return solution;
    }

    public IrrationalNumber getPositiveNumber()
    {
        if(getIntegerPartOfIrrationalNumber(firstDecision) > 0)
            return firstDecision;
        else
            return secondDecision;
    }

    public IrrationalNumber getNegativeNumber()
    {
        if(getIntegerPartOfIrrationalNumber(firstDecision) < 0)
            return firstDecision;
        else
            return secondDecision;
    }

    public static int getIntegerPartOfIrrationalNumber(IrrationalNumber irrationalNumber)
    {
        double firstMultiplier = ((double) irrationalNumber.d) * Math.sqrt(irrationalNumber.b);
        return (int)( (((double)irrationalNumber.a) + firstMultiplier) / ((double)irrationalNumber.c) );
    }
}
