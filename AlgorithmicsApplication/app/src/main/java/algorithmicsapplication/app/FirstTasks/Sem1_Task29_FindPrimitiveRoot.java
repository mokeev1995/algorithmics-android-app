package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 08/01/2015.
 */
public class Sem1_Task29_FindPrimitiveRoot extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task29);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task29_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Найти ПРИМИТИВНЫЙ по модулю n:");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите число n: ");
        layout.addView(enterNumberA);
        EditText inputBaseNumberABox = new EditText(this);
        inputBaseNumberABox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberABox);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBaseNumberABox, this));
    }

    public class CustomClickListener implements View.OnClickListener
    {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText numberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText numberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.numberInputBox = numberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

            int inputNumber;
            try
            {
                inputNumber = Integer.parseInt(numberInputBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }


            try
            {
                String solution = "Найдем значение функции Эйлера от этого числа. Образующая в этой степени должна обращаться"
                        +" в единицу " + "по данному модулю.\n";
                int groupOrder = PrimeFactorization.eulerFunction(inputNumber);
                solution += "Значение функции Эйлера равно:" + groupOrder + "; \n";
                solution += "Поэтому необходимо искать элемент порядка '" + groupOrder +"' (т.е. чтобы в этой степени он обращался в 1); \n";
                solution += "-----------------------" + "\n"+ "\n";
                for(int i = 2; i < inputNumber; i++)
                {
                    int previosRest = i;
                    boolean isFindGeneratrix = false;
                    for(int j = 2; j <= groupOrder; j++)
                    {
                        previosRest = (previosRest * i) % inputNumber;
                        solution += i + "^" + j + "==" + previosRest + "(mod " + inputNumber + "); \n";
                        if (previosRest == 1 && j == groupOrder)
                        {
                            solution += "Найден примитивный корень: '" + i + "'; \n";
                            isFindGeneratrix = true;
                            break;
                        }
                        if(previosRest == 1 && j == groupOrder)
                            solution += i + " не является примитивным корнем, т.к. в степени '"  + groupOrder + "' - не обращается в 1.\n";
                        if (previosRest == 1 && j < groupOrder)
                        {
                            solution += i + " не является примитивным корнем, обращается в единицу в меньшей степени, чем '"  + groupOrder + "' \n";
                            break;
                        }
                    }
                    if(isFindGeneratrix)
                        break;
                }

                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
    }
}