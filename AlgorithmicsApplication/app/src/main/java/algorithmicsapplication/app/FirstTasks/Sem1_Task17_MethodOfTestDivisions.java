package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 06/01/2015.
 */
public class Sem1_Task17_MethodOfTestDivisions extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task17);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task17_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Методом пробных делений найти все простые числа:");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите НИЖНЮЮ границу ");
        layout.addView(enterNumberA);
        EditText inputBoxFirstNumberA = new EditText(this);
        inputBoxFirstNumberA.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxFirstNumberA);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView enterNumberP = new TextView(this);
        enterNumberP.setText("Введите ВЕРХНЮЮ границу: ");
        layout.addView(enterNumberP);
        EditText inputBoxSecondNumberB = new EditText(this);
        inputBoxSecondNumberB.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBoxSecondNumberB);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBoxFirstNumberA, inputBoxSecondNumberB, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText secondNumberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, EditText secondNumberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.secondNumberInputBox = secondNumberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            int firstNumber;
            int secondNumber;
            try
            {
                firstNumber = Integer.parseInt(firstNumberInputBox.getText().toString());
                secondNumber = Integer.parseInt(secondNumberInputBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                Integer amountOfOddNumberInCurrentInterval = ((secondNumber - firstNumber) / 2 + 1);// индекс k
                String solution = "Необходимо найти простые числа в диапозоне: [" + firstNumber + ", " + secondNumber + "];\n\n";
                solution += "Всего в этом дипозоне: " + amountOfOddNumberInCurrentInterval
                        + " НЕЧЕТНЫХ чисел (в пособии это числа равно 'k'). Поэтому"
                        + " индекс j - не должен превышать числа '" + amountOfOddNumberInCurrentInterval + "';\n\n";
                ArrayList<NumberWithIdentifier> sourceOddNumbers = new ArrayList<NumberWithIdentifier>();
                for (int i = firstNumber; i <= secondNumber; i++) {
                    //добавляем все нечетные
                    if (i % 2 != 0) {
                        sourceOddNumbers.add(new NumberWithIdentifier(i, true));
                    }
                }

                solution += "Первым делителем всегда берется 3.\n";
                int currentTestDivider = 3;

                while (currentTestDivider < Math.sqrt(secondNumber)) {
                    solution += "\n-----------------\n";

                    Integer restOfDivisionFirstNumberByCurrentTestDivider = firstNumber % currentTestDivider;
                    solution += "Остаток от деления " + firstNumber + " на текущий пробный делитель '" +
                            +currentTestDivider + "' равен: " + restOfDivisionFirstNumberByCurrentTestDivider + ", т.к.: " + firstNumber + " = " + currentTestDivider
                            + " * " + (firstNumber / currentTestDivider) + " + " + restOfDivisionFirstNumberByCurrentTestDivider + ";\n";

                    Integer jIndex = 0;
                    if (firstNumber % currentTestDivider == 0 && firstNumber > currentTestDivider) {
                        // ЕСЛИ M > D и D | M
                        sourceOddNumbers.get(0).isPrime = false;
                        solution += "Т.е. нижняя граница '" + firstNumber + "' - является составным" + ".\n";
                        jIndex = 1;
                    } else {
                        if(firstNumber <= currentTestDivider)
                        {
                            jIndex = getIndexOfItemInList(sourceOddNumbers, currentTestDivider) + 1 + currentTestDivider;
                            solution += "Т.к. нижняя граница не превышает текущий делитель: " + firstNumber + " <= " +
                                    currentTestDivider + ", то текущий делитель лежит в интервале и составными будут каждое d-ое следующее за ним число'"
                                    + currentTestDivider + "'.\n";
                        }
                        else
                        {
                            if (restOfDivisionFirstNumberByCurrentTestDivider % 2 != 0) {
                                jIndex = 1 + ((currentTestDivider - restOfDivisionFirstNumberByCurrentTestDivider) / 2);
                                solution += "Т.к. этот остаток - НЕЧЕТНЫЙ, то индекс все индекс вычисляется по формуле: j = 1 + d - r/2 = " +
                                        "1 + " + currentTestDivider + " - ( " + restOfDivisionFirstNumberByCurrentTestDivider
                                        + " ) / 2"
                                        + " = " + jIndex + ";\n";

                            } else if (restOfDivisionFirstNumberByCurrentTestDivider % 2 == 0) {
                                jIndex = 1 + currentTestDivider - (restOfDivisionFirstNumberByCurrentTestDivider / 2);
                                solution += "Т.к. этот остаток - ЧЕТНЫЙ, то индекс вычисляется по формуле: j = 1 + (d-r)/2 = " +
                                        "1 + (" + currentTestDivider + " - " + restOfDivisionFirstNumberByCurrentTestDivider
                                        + ") / 2"
                                        + " = " + jIndex + ";\n";
                            }
                        }
                    }

                    solution += "Для данного пробного делителя '" + currentTestDivider + "'  - список индексов" +
                            " чисел являющиеся составными будет включать j=: ";
                    ArrayList<Integer> listOfIndexesJ = new ArrayList<Integer>();
                    for (int i = jIndex; i <= amountOfOddNumberInCurrentInterval; i += currentTestDivider) {
                        listOfIndexesJ.add(i);
                        if (i != jIndex) solution += ", ";

                        solution += i;
                    }
                    solution += ". Т.е. мы к полученному индексу прибавляем текущий пробный делитель, до тех пор,"
                            + " пока не превысит число всех нечетых чисел в этом диапозоне: '" +
                            amountOfOddNumberInCurrentInterval + "'.";

                    solution += "\n\nЧисла являющиеся СОСТАВНЫМИ для данного пробного делителя '" + currentTestDivider + "': ";
                    for (int i = 0; i < listOfIndexesJ.size(); i++) {
                        Integer currentCompositeNumber = firstNumber + 2 * listOfIndexesJ.get(i) - 2;
                        NumberWithIdentifier currentNumber = sourceOddNumbers.get(getIndexOfItemInList(sourceOddNumbers, currentCompositeNumber));
                        currentNumber.isPrime = false;
                        solution += currentNumber.number + ", ";
                    }
                    solution+= ".\n";

                    if ((currentTestDivider - 1) % 6 == 0) {
                        solution += "Текущий делитель d = " + currentTestDivider +
                                " представим в виде 6t+1 " + ", поэтому следующий пробный делитель (d+2) можно не просматривать, т.к. все составные"
                                + " были вычеркнуты делящиеся на 3" + "\n Поэтому увеличиваем текущий пробный делитель: '"
                                + currentTestDivider + "' на 4. d:= " + (currentTestDivider + 4) + "\n";
                        currentTestDivider += 4;
                    } else {
                        solution += "\n\nУвеличивает текущий пробный делитель на 2: d := " + currentTestDivider + "+2 = " +
                                (currentTestDivider + 2) + ";\n";
                        currentTestDivider += 2;
                    }

                    if (currentTestDivider >= Math.sqrt(secondNumber)) {
                        solution += "\n\nТ.к. текущий пробный делитель: '" + currentTestDivider + " >=" + " sqrt("
                                + secondNumber + ") = '" + Math.sqrt(secondNumber) + "' - больше значение квадратного корня верхней границы интервала, то выполнение алгоритма прекращается.\n";
                    }
                }

                solution += "------------\n\nВ ИТОГЕ ПОЛУЧАЕМ ВСЕ ПРОСТЫЕ ЧИСЛА В ЭТОМ ИНТЕРВАЛЕ:\n";
                for (int i = 0; i < sourceOddNumbers.size(); i++) {
                    if (sourceOddNumbers.get(i).isPrime)
                        solution += sourceOddNumbers.get(i).number + ", ";
                }

                solution += "\n\n\n(ТОЛЬКО ДЛЯ ПРОВЕРКИ КОРРЕКТНОСТИ РАБОТЫ АЛГОРИТМА ПРОБНЫХ ДЕЛЕНИЙ): проверка на простоту полным перебором (в решении естественно не записывать это!!):\n";
                for (int i = 0; i < sourceOddNumbers.size(); i++) {
                    if (PrimeFactorization.isPrimeNumber(sourceOddNumbers.get(i).number)) {
                        solution += sourceOddNumbers.get(i).number + ", ";
                    }
                }

                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }

        private int getIndexOfItemInList(ArrayList<NumberWithIdentifier> list, int item) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).number == item)
                    return i;
            }
            return -1;
        }

        private class NumberWithIdentifier {
            int number;
            boolean isPrime = true;

            private NumberWithIdentifier(int number, boolean isPrime) {
                this.number = number;
                this.isPrime = isPrime;
            }
        }
    }
}
