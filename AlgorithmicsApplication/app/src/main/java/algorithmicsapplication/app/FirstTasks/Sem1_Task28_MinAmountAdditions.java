package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 04/01/2015.
 */
public class Sem1_Task28_MinAmountAdditions extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task28);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task28_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Найти минимально число сложений для вычисления A * B");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите первое число: ");
        layout.addView(enterNumberA);
        EditText inputBaseNumberABox = new EditText(this);
        inputBaseNumberABox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberABox);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        TextView enterNumberP = new TextView(this);
        enterNumberP.setText("Введите второе число: ");
        layout.addView(enterNumberP);
        EditText inputBaseNumberPBox = new EditText(this);
        inputBaseNumberPBox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberPBox);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBaseNumberABox, inputBaseNumberPBox, this));
    }

    public class CustomClickListener implements View.OnClickListener
    {
        LinearLayout layout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText secondNumberInputBox;
        Context context;

        public CustomClickListener(LinearLayout layout, TextView resultTextView, EditText firstNumberInputBox, EditText secondNumberInputBox, Context context) {
            this.layout = layout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.secondNumberInputBox = secondNumberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            int firstNumber;
            int secondNumber;
            try
            {
                firstNumber = Integer.parseInt(firstNumberInputBox.getText().toString());
                secondNumber = Integer.parseInt(secondNumberInputBox.getText().toString());
                if(firstNumber < secondNumber)
                {
                    int temp = firstNumber;
                    firstNumber = secondNumber;
                    secondNumber = temp;
                }
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                if(getAmountAdditions(firstNumber) < getAmountAdditions(secondNumber))
                    secondNumber = firstNumber;

                String secondBinaryMultiplier = Integer.toBinaryString(secondNumber);
                int[] arrayAi = new int[secondBinaryMultiplier.length()];
                String solution = "Число '" + secondNumber + "' в двоичном формате: " + secondBinaryMultiplier + ";" + "\n";
                arrayAi[0] = firstNumber;
                solution += "a[0] = " + firstNumber +  ";" + "\n";
                for(int i = 1; i < arrayAi.length; i++)
                {
                    arrayAi[i] = arrayAi[i-1] + arrayAi[i-1];
                    solution += "a[" + i + "] = a[" + (i-1)+ "]+a["+ (i-1)+ "] = " + arrayAi[i] +  ";" + "\n";
                }

                String invertedBinaryNumber = new StringBuilder(secondBinaryMultiplier).reverse().toString();
                int multiplicationResult = 0;
                solution += "RESULT: ";
                for(int i = 0; i < secondBinaryMultiplier.length(); i++)
                {
                    if(invertedBinaryNumber.charAt(i) == '1')
                    {
                        multiplicationResult += arrayAi[i];
                        solution += "a[" + i + "]";
                        if(i != secondBinaryMultiplier.length()-1)
                            solution += " + ";
                    }
                }
                solution += " = " + multiplicationResult +  ";" + "\n";


                int amountAddition = 0;
                amountAddition += secondBinaryMultiplier.length() - 1;
                for(int i = 0; i < secondBinaryMultiplier.length(); i++)
                {
                    if(secondBinaryMultiplier.charAt(i) == '1')
                    {
                        amountAddition++;
                    }
                }
                amountAddition--;// на одну меньше, чем единиц во втором числе
                solution += "\nЧисло сложений: " + amountAddition;

                resultTextView.setText("Ход решения: " + solution);

                layout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }

        public int getAmountAdditions(int number)
        {
            String secondBinaryMultiplier = Integer.toBinaryString(number);
            int amount = 0;
            for(int i = 0; i < secondBinaryMultiplier.length(); i++)
            {
                if(secondBinaryMultiplier.charAt(i) == 1)
                    amount++;
            }
            amount--;
            amount += secondBinaryMultiplier.length()-1;
            return amount;
        }
    }
}
