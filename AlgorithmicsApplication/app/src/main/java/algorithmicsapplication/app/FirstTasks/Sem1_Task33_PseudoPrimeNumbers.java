package algorithmicsapplication.app.FirstTasks;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Pair;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import algorithmicsapplication.app.R;

/**
 * Created by Nick on 04/01/2015.
 */
public class Sem1_Task33_PseudoPrimeNumbers extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.sem1_task33);

        LinearLayout layout = (LinearLayout) findViewById(R.id.sem1_task33_layout);
        TextView resultTextView = new TextView(this);
        TextView taskCondition = new TextView(this);
        taskCondition.setText("Определить, является ли данное 'n' - по основанию 'a': 1) псевдопростым; 2) сильнопсевдопростым?");
        TextView taskTheory = new TextView(this);
        taskTheory.setText("ТЕОРИЯ");
        layout.addView(taskCondition);
        layout.addView(taskTheory);
        //----------------------------

        TextView enterNumberA = new TextView(this);
        enterNumberA.setText("Введите число b, являющееся ОСНОВАНИЕМ: ");
        layout.addView(enterNumberA);
        EditText inputBaseNumberABox = new EditText(this);
        inputBaseNumberABox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberABox);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);


        TextView enterNumberP = new TextView(this);
        enterNumberP.setText("Введите число n, проверяемое на псевдопростоту: ");
        layout.addView(enterNumberP);
        EditText inputBaseNumberPBox = new EditText(this);
        inputBaseNumberPBox.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
        layout.addView(inputBaseNumberPBox);

        Button calculateButton = new Button(this);
        calculateButton.setText("Calculate");
        layout.addView(calculateButton);
        calculateButton.setOnClickListener(new CustomClickListener(layout, resultTextView, inputBaseNumberABox, inputBaseNumberPBox, this));
    }

    public class CustomClickListener implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView resultTextView;
        EditText firstNumberInputBox;
        EditText secondNumberInputBox;
        Context context;

        public CustomClickListener(LinearLayout linearLayout, TextView resultTextView, EditText firstNumberInputBox, EditText secondNumberInputBox, Context context) {
            this.linearLayout = linearLayout;
            this.resultTextView = resultTextView;
            this.firstNumberInputBox = firstNumberInputBox;
            this.secondNumberInputBox = secondNumberInputBox;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);

            int baseNumber;
            int suspectedPrimeNumber;
            try
            {
                baseNumber = Integer.parseInt(firstNumberInputBox.getText().toString());
                suspectedPrimeNumber = Integer.parseInt(secondNumberInputBox.getText().toString());
            }
            catch (Exception  e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Некорректные входные данные!");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }

            try
            {
                String solution = "";
                solution += "\n-----------------\nПРОВЕРКА ПСЕВДОПРОСТОТЫ: \n-----------------\n";
                solution += FindPseudoPrime(baseNumber, suspectedPrimeNumber);
                solution += "\n-----------------\nПРОВЕРКА СИЛЬНОЙ ПСЕВДОПРОСТОТЫ: \n-----------------\n";
                solution += FindStronglyPseudoPrime(baseNumber, suspectedPrimeNumber);
                resultTextView.setText(solution);
                linearLayout.addView(resultTextView);
            }
            catch (Exception e)
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Ошибка в работе алгоритма.");
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return;
            }
        }
        
        private String FindPseudoPrime(int baseNumber, int suspectedPrimeNumber)
        {
            String solution = "Необходимо проверить, что число '" + suspectedPrimeNumber + "' - является составным, и что оно ведет себя"
                    + " как простое число в теореме Ферма, т.е. что для данного а выполяется: b^("
                    + suspectedPrimeNumber + " - 1) = " + baseNumber + "^(" + + suspectedPrimeNumber + " - 1)"
                    +  " = 1(mod" + suspectedPrimeNumber +").\n\n";
            if(!PrimeFactorization.isPrimeNumber(suspectedPrimeNumber))
            {
                List<Pair<Integer, Integer>> degreesList = PrimeFactorization.factorizeNumberToPrimeFactors(suspectedPrimeNumber);
                solution += "Число '" + suspectedPrimeNumber + "' - является СОСТАВНЫМ и имеет следующее разложение на простые множители: '" + suspectedPrimeNumber + "' = ";
                for(int i = 0; i < degreesList.size(); i++)
                {
                    Pair<Integer, Integer> currentPair = degreesList.get(i);
                    solution += currentPair.first + "^" + currentPair.second;
                    if(i != degreesList.size() - 1)
                        solution +=  " * ";
                }
                solution += "\n\n";

                if(isHasPowOfOneBetweenDivisors(baseNumber, suspectedPrimeNumber - 1, suspectedPrimeNumber))
                {
                    solution += "Т.к. выполняется такое соотношение:\n";
                    int minPowOfOne = findMinPowOfOne(baseNumber, suspectedPrimeNumber -1, suspectedPrimeNumber);
                    int previousRest = baseNumber;
                    for(int i = 2; i <= minPowOfOne; i++)
                    {
                        previousRest = (previousRest * baseNumber) % suspectedPrimeNumber;
                        solution += baseNumber + "^" + i + " = " + previousRest + " (mod " + suspectedPrimeNumber + "); \n";
                    }
                    solution += "То соотвественно имеем: \n";
                    solution +=  "(" + baseNumber + ")^("+suspectedPrimeNumber + " - 1)" + " = " + "(" + baseNumber + ")^"+(suspectedPrimeNumber-1) + " = ((" + baseNumber + ")^"
                            + minPowOfOne + ")^" + ((suspectedPrimeNumber-1)/ minPowOfOne) +" = 1 (mod " + suspectedPrimeNumber + ") - т.е. является составным \n"
                    + ", но ведет себя как простое число в теореме Ферма.\n";
                }
                else
                {
                    solution += "Число " + baseNumber + " не имеет среди делителей " + (suspectedPrimeNumber -1) + " степеней, которые обращаются в единицу! \n";
                    if(suspectedPrimeNumber % baseNumber != 0)
                    {
                        solution += "Выполяеется условие, что число " + suspectedPrimeNumber + " не делится на число "  + baseNumber;
                        solution += "\nПодробное возведение в степень " + suspectedPrimeNumber + "- 1; \n";
                        int previousRest = baseNumber;
                        for(int i = 2; i <= suspectedPrimeNumber - 1; i++)
                        {
                            previousRest = (previousRest * baseNumber) % suspectedPrimeNumber;
                            solution += "b^i (mod m) = " + baseNumber + "^" + i + "(mod " + suspectedPrimeNumber + ") = " + previousRest +"; \n";
                        }
                        if(previousRest == 1)
                        {
                            solution += "Число " + suspectedPrimeNumber + " является псевдопростым по основанию " + baseNumber
                                    + "т.к. " + baseNumber + "^(" + suspectedPrimeNumber + "-1)" + "= 1 (mod " + suspectedPrimeNumber + "); \n";
                        }
                        else
                        {
                            solution += "Число " + suspectedPrimeNumber + "НЕ является псевдопростым по основанию " + baseNumber
                                    + "т.к. " + baseNumber + "^(" + suspectedPrimeNumber + "-1)" + "!= 1 (mod " + suspectedPrimeNumber + "); \n";
                        }
                    }
                    solution += "Число " + suspectedPrimeNumber + "НЕ является псевдопростым по основанию " + baseNumber
                            + "т.к. число" +  suspectedPrimeNumber + "делит основание: " + baseNumber +"; \n";
                }
            }
            else
            {
                solution = "Число " + suspectedPrimeNumber + " является простым, т.е. НЕ является 'псевдопростым'";
            }
            return solution;
        }

        private int findMinPowOfOne(int base, int pow, int mod) {
            int previousRest = base;
            for (int i = 2; i <= pow; i++) {
                previousRest = (previousRest * base) % mod;
                if (previousRest == 1) {
                    return i;
                }
            }
            return -1;
        }


        private boolean isHasPowOfOneBetweenDivisors(int base, int pow, int mod)
        {
            List<Integer> powOfOne = new ArrayList<Integer>();
            int previousRest = base;
            for(int i = 2; i <= pow; i++)
            {
                previousRest = (previousRest * base) % mod;
                if(previousRest == 1)
                {
                    powOfOne.add(i);
                }
            }
            for(int i = 0; i < powOfOne.size(); i++)
            {
                if(pow % powOfOne.get(i) == 0)
                {
                    return true;
                }
            }
            return false;
        }
        
        private String FindStronglyPseudoPrime(int baseNumber, int suspectedPrimeNumber)
        {
            String solution = "Для проверяемого числа n = " + suspectedPrimeNumber + " - мы рассматриваем такое разложение: "
                    + "(n-1) = (" + suspectedPrimeNumber + " - 1) = " + (suspectedPrimeNumber - 1)
                    + " = (2^s) * q, где q - нечетное. Для того, чтобы n = " + suspectedPrimeNumber + " - являлось 'сильнопсевдопростым по основанию"
                    + "'" + baseNumber + "' - необходимо, чтобы выполнялись условия:"
                    +"\nЛИБО выполяется b^q = 1(mod n);" +
                    " \nЛИБО существует i: 0 <= i <= (s-1), такое что: b^(2^(i) * q) = (-1) (mod n)\n\n";
            ArrayList<Pair<Integer, Integer>> degrees = PrimeFactorization.factorizeNumberToPrimeFactors((suspectedPrimeNumber-1));

            boolean isFindPrime = false;
            if(degrees.size() >= 1) {
                if (degrees.get(0).first == 2) {
                    int sDegree = degrees.get(0).second;
                    int qValue = 1;
                    for (int i = 1; i < degrees.size(); i++) {
                        qValue *= PrimeFactorization.raiseToPow(degrees.get(i).first, degrees.get(i).second);
                    }

                    solution += "Число '(" + suspectedPrimeNumber + " - 1)' представимо в виде : " + (suspectedPrimeNumber-1) + " = " + "2^" + sDegree + " * " + qValue + "; \n";
                    solution += "Где q = " + qValue + ", S = " + sDegree + ", b =" + baseNumber + ", n = " + suspectedPrimeNumber + ". \n\n";

                    int previousRest = baseNumber;
                    for (int i = 2; i <= qValue; i++) {
                        int intermediateResult = (previousRest * baseNumber);
                        previousRest = intermediateResult % suspectedPrimeNumber;
                        solution += baseNumber + "^" + i + "(mod " + suspectedPrimeNumber + ") = " +  baseNumber +
                                "^(" + i + "-1) * " + baseNumber + " = "
                                + intermediateResult + " = " + previousRest + "; \n";
                    }
                    if (previousRest == 1) {
                        isFindPrime = true;
                        solution += suspectedPrimeNumber + " ЯВЛЯЕТСЯ сильнопсевдопростым, т.к. ВЫПОЛНИЛОСЬ ПЕРВОЕ УСЛОВИЕ: " + baseNumber + "^" + qValue +
                                "= 1(mod " + suspectedPrimeNumber + "); \n";
                    } else {
                        solution += "Первое условие НЕ ВЫПОЛНИЛОСЬ  т.к.: " + baseNumber + "^" + qValue +
                                " != 1(mod " + suspectedPrimeNumber + ").\n(В таком случае, если выполнится второе условие, то оно будет 'сильнопсевдопростым'. "
                                + "Если оба условия не выполнются, то заданное число НЕ является 'cильнопсевдопростым'.) \n";
                    }

                    solution += "\n\n--------------\nТеперь проверим, выполняется ли второе условие (если выполнилось первое, то для 'сильнопсевдопростоты"
                            + "' - второе условие является уже не обязательным).\n\n";

                    for (int i = 0; i <= sDegree - 1; i++) {
                        solution += "Просматриваем i = " + i + ": \n";
                        int powOfTwo = PrimeFactorization.raiseToPow(2, i);
                        int twoOnQValue = qValue * powOfTwo;
                        solution += "2^i =" + "2^" + i + "=" + powOfTwo + "; (2^i) * q = 2^" + i + "*" + qValue + "=" + twoOnQValue + "; \n";
                        solution += "Подробное возведение числа '"  + baseNumber + "' до степени " + "(2^i * q) = '"+ twoOnQValue + "':" + "\n\n";
                        int prevRestSecondCondition = baseNumber;
                        for (int j = 2; j <= twoOnQValue; j++) {
                            prevRestSecondCondition = (prevRestSecondCondition * baseNumber) % suspectedPrimeNumber;
                            solution += baseNumber + "^" + j + "(mod " + suspectedPrimeNumber + ") = " + prevRestSecondCondition + "; \n";
                        }
                        if (prevRestSecondCondition == (suspectedPrimeNumber - 1)) {
                            isFindPrime = true;
                            solution += "Число '" + suspectedPrimeNumber + "' - является 'сильнопсевдпростым' по основанию (ПО ВТОРОМУ УСЛОВИЮ)'" +
                                    baseNumber + "', т.к. нашлось такое i = " + i + " такое, что: b^( (2^i) * q) = "
                                    + baseNumber + "^( (2^" + i + ") * q)" + " = " + + baseNumber + "^( (2^" + i + ") * " + qValue + ")"
                                    + " = (-1)(mod " + suspectedPrimeNumber + "); \n";
                            break;
                        }
                        else
                        {
                            if(i != (sDegree-1))
                                solution += "Т.к. для данного i = " + i + " - не выполнилось условие, что (b)^( (2^i) * q) = -1(mod n), то проверяем следующую i = "
                                        + (i+1) + "\n\n";
                        }
                    }
                    if (!isFindPrime) {
                        solution += "Число '" + suspectedPrimeNumber + "' по основанию '" + baseNumber + "' - НЕ является 'сильнопсевдопростым'!";
                    }
                }
            }
            return solution;
        }
    }
}
