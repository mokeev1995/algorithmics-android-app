package algorithmicsapplication.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import algorithmicsapplication.app.FirstTasks.JavaScriptMathRender;
import algorithmicsapplication.app.FirstTasks.ListOfTasks;
import algorithmicsapplication.app.FirstTasks.Sem1_OrdersOfAllElementsInGroup;
import algorithmicsapplication.app.FirstTasks.Sem1_Task10_HideIrrationalFraction;
import algorithmicsapplication.app.FirstTasks.Sem1_Task11_FindIrrationalNumberByFractionAndQuotaiont;
import algorithmicsapplication.app.FirstTasks.Sem1_Task13_EvaluateAccuracyApproximationGivenAppropriateFration;
import algorithmicsapplication.app.FirstTasks.Sem1_Task14_EvaluateAccuraceAppriopirativeIrrationalFraction;
import algorithmicsapplication.app.FirstTasks.Sem1_Task15_QuotientAndRestGaussianNumbers;
import algorithmicsapplication.app.FirstTasks.Sem1_Task16_GcdGaussianNumbers;
import algorithmicsapplication.app.FirstTasks.Sem1_Task17_MethodOfTestDivisions;
import algorithmicsapplication.app.FirstTasks.Sem1_Task18_ExtendedEuclideanAlgorithmSolvingComparison;
import algorithmicsapplication.app.FirstTasks.Sem1_Task19_AllSolutionsOfComparison;
import algorithmicsapplication.app.FirstTasks.Sem1_Task20_FindInvertedElementByEuclidAlgorithm;
import algorithmicsapplication.app.FirstTasks.Sem1_Task21_SystemComparisons;
import algorithmicsapplication.app.FirstTasks.Sem1_Task22_SystemComparisonByFormula;
import algorithmicsapplication.app.FirstTasks.Sem1_Task23_EulerFunction;
import algorithmicsapplication.app.FirstTasks.Sem1_Task24_MobiusFunction;
import algorithmicsapplication.app.FirstTasks.Sem1_Task25_FermatTheorem;
import algorithmicsapplication.app.FirstTasks.Sem1_Task26_EulerTheorem;
import algorithmicsapplication.app.FirstTasks.Sem1_Task27_MinMultiplications;
import algorithmicsapplication.app.FirstTasks.Sem1_Task28_MinAmountAdditions;
import algorithmicsapplication.app.FirstTasks.Sem1_Task29_FindPrimitiveRoot;
import algorithmicsapplication.app.FirstTasks.Sem1_Task30_FindAllPrimitiveRoots;
import algorithmicsapplication.app.FirstTasks.Sem1_Task31_CyclicGroup;
import algorithmicsapplication.app.FirstTasks.Sem1_Task32_DescribeStructureMultiplicativeGroup;
import algorithmicsapplication.app.FirstTasks.Sem1_Task33_PseudoPrimeNumbers;
import algorithmicsapplication.app.FirstTasks.Sem1_Task34_StandartSetOfRests;
import algorithmicsapplication.app.FirstTasks.Sem1_Task4_FindGreatestCommonDivisor;
import algorithmicsapplication.app.FirstTasks.Sem1_Task5_BinaryGreatestCommonDivisor;
import algorithmicsapplication.app.FirstTasks.Sem1_Task6_FindAllInteferSolutionOfEquation;
import algorithmicsapplication.app.FirstTasks.Sem1_Task7_ContinuedFraction;
import algorithmicsapplication.app.FirstTasks.Sem1_Task8_HideContinuedFraction;
import algorithmicsapplication.app.FirstTasks.Sem1_Task9_IrrationalToContinuedFraction;

/**
 * Created by Nick on 04/01/2015.
 */
public class FirstPartTasksMenu extends ActionBarActivity {

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.first_part_tasks_menu);

        AddTaskButton("Java-Script Math-Rendering", JavaScriptMathRender.class);
        AddTaskButton("Формулировки задач", ListOfTasks.class);
        AddTaskButton("Найти порядок каждого элемента в группе", Sem1_OrdersOfAllElementsInGroup.class);
        AddTaskButton("№ 4", Sem1_Task4_FindGreatestCommonDivisor.class);
        AddTaskButton("№ 5", Sem1_Task5_BinaryGreatestCommonDivisor.class);
        AddTaskButton("№ 6", Sem1_Task6_FindAllInteferSolutionOfEquation.class);
        AddTaskButton("№ 7", Sem1_Task7_ContinuedFraction.class);
        AddTaskButton("№ 8", Sem1_Task8_HideContinuedFraction.class);
        AddTaskButton("№ 9", Sem1_Task9_IrrationalToContinuedFraction.class);
        AddTaskButton("№ 10", Sem1_Task10_HideIrrationalFraction.class);
        AddTaskButton("№ 11", Sem1_Task11_FindIrrationalNumberByFractionAndQuotaiont.class);
        AddTaskButton("№ 13", Sem1_Task13_EvaluateAccuracyApproximationGivenAppropriateFration.class);
        AddTaskButton("№ 14", Sem1_Task14_EvaluateAccuraceAppriopirativeIrrationalFraction.class);
        AddTaskButton("№ 15", Sem1_Task15_QuotientAndRestGaussianNumbers.class);
        AddTaskButton("№ 16", Sem1_Task16_GcdGaussianNumbers.class);
        AddTaskButton("№ 17", Sem1_Task17_MethodOfTestDivisions.class);
        AddTaskButton("№ 18", Sem1_Task18_ExtendedEuclideanAlgorithmSolvingComparison.class);
        AddTaskButton("№ 19", Sem1_Task19_AllSolutionsOfComparison.class);
        AddTaskButton("№ 20", Sem1_Task20_FindInvertedElementByEuclidAlgorithm.class);
        AddTaskButton("№ 21", Sem1_Task21_SystemComparisons.class);
        AddTaskButton("№ 22", Sem1_Task22_SystemComparisonByFormula.class);
        AddTaskButton("№ 23", Sem1_Task23_EulerFunction.class);
        AddTaskButton("№ 24", Sem1_Task24_MobiusFunction.class);
        AddTaskButton("№ 25", Sem1_Task25_FermatTheorem.class);
        AddTaskButton("№ 26", Sem1_Task26_EulerTheorem.class);
        AddTaskButton("№ 27", Sem1_Task27_MinMultiplications.class);
        AddTaskButton("№ 28", Sem1_Task28_MinAmountAdditions.class);
        AddTaskButton("№ 29", Sem1_Task29_FindPrimitiveRoot.class);
        AddTaskButton("№ 30", Sem1_Task30_FindAllPrimitiveRoots.class);
        AddTaskButton("№ 31", Sem1_Task31_CyclicGroup.class);
        AddTaskButton("№ 32", Sem1_Task32_DescribeStructureMultiplicativeGroup.class);
        AddTaskButton("№ 33", Sem1_Task33_PseudoPrimeNumbers.class);
        AddTaskButton("№ 34", Sem1_Task34_StandartSetOfRests.class);
    }

    public void AddTaskButton(String textButton, Class layoutClass)
    {
        Button taskButton = new Button(this);
        taskButton.setText(textButton);
        LinearLayout layout = (LinearLayout) findViewById(R.id.firstPartTasksMenu_layout);
        layout.addView(taskButton);
        taskButton.setOnClickListener(new NumberTaskClickListener(layoutClass));
    }

    public class NumberTaskClickListener implements View.OnClickListener {

        Class actionBarActivity;

        public NumberTaskClickListener(Class actionBarActivity) {
            this.actionBarActivity = actionBarActivity;
        }

        @Override
        public void onClick(View view) {
            startActivity(new Intent(FirstPartTasksMenu.this, actionBarActivity));
        }
    }
}
